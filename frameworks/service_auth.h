/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _SERVICE_AUTH_H_
#define _SERVICE_AUTH_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "service_stdinc.h"
#include "service_string.h"
#include "service_sha256.h"
#include "tiot_state_api.h"
#include "tiot_sysdep_api.h"

#define SERVICE_AUTH_SDK_VERSION "sdkversion"
#define SERVICE_AUTH_TIMESTAMP   "1631235555507"
#define SERVICE_AUTH_ENCRYPTION_TYPE   "hmacsha256"
#define SERVICE_AUTH_CLIENT_ID   "talkweb"

int32_t service_auth_tls_psk(tiot_sysdep_portfile_t *sysdep, char **psk_id, char psk[65], char *product_key, char *device_name, char *device_secret);
int32_t service_auth_mqtt_username(tiot_sysdep_portfile_t *sysdep, char **dest, char *product_key, char *device_name);
int32_t service_auth_mqtt_password(tiot_sysdep_portfile_t *sysdep, char **dest, uint64_t auth_timestamp, char *product_key, char *device_name, char *device_secret, uint8_t assigned_clientid);
int32_t service_auth_mqtt_clientid(tiot_sysdep_portfile_t *sysdep, char **dest, uint64_t auth_timestamp, char *product_key, char *device_name, char *secure_mode, char *extend_clientid, uint8_t assigned_clientid);
int32_t service_auth_http_body(tiot_sysdep_portfile_t *sysdep, char **dest, char *product_key, char *device_name, char *device_secret);

#if defined(__cplusplus)
}
#endif

#endif

