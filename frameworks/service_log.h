/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _SERVICE_LOG_H_
#define _SERVICE_LOG_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdio.h>
#include "service_stdinc.h"
#include "service_string.h"
#include "tiot_state_api.h"
#include "tiot_sysdep_api.h"

#define DE_LOG_PRINT 1 // log输出宏

extern tiot_state_logcb_t g_logcb_ctx;
static const char *level_strings[] = {
    "DEBUG", "INFO", "WARN", "ERROR"
};

#if DE_LOG_PRINT

enum {
    D_DEBUG_LEVEL = 0,
    D_INFO_LEVEL = 1,
    D_WARN_LEVEL = 2,
    D_ERROR_LEVEL = 3,
    D_MAX_LEVEL = 4,
};

#define log_print(level, fmt, ...)  do {\
        int dbg_level = level;\
        if((dbg_level < D_MAX_LEVEL && g_logcb_ctx != NULL)){\
            printf("[%s][%s][%d]" fmt, level_strings[level], __FILE__, __LINE__, ##__VA_ARGS__); \
        }\
    } while (0)

#define log_debug(...) log_print(D_DEBUG_LEVEL, __VA_ARGS__)
#define log_info(...) log_print(D_INFO_LEVEL, __VA_ARGS__)
#define log_warn(...) log_print(D_WARN_LEVEL, __VA_ARGS__)
#define log_error(...) log_print(D_ERROR_LEVEL, __VA_ARGS__)

#else
#define log_debug(...)
#define log_info(...)
#define log_warn(...)
#define log_error(...)
#endif

uint64_t service_log_get_timestamp(tiot_sysdep_portfile_t *sysdep);

#if defined(__cplusplus)
}
#endif

#endif

