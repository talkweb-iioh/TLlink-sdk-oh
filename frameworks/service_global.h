/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _SERVICE_GLOBAL_H_
#define _SERVICE_GLOBAL_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "service_stdinc.h"
#include "tiot_state_api.h"
#include "tiot_sysdep_api.h"

int32_t service_global_init(tiot_sysdep_portfile_t *sysdep_param);
int32_t service_global_tlink_id_next(tiot_sysdep_portfile_t *sysdep_param, int32_t *tlink_id);
int32_t service_global_set_backup_ip(tiot_sysdep_portfile_t *sysdep_param, char ip[16]);
int32_t service_global_get_backup_ip(tiot_sysdep_portfile_t *sysdep_param, char ip[16]);
int32_t service_global_deinit(tiot_sysdep_portfile_t *sysdep_param);

#if defined(__cplusplus)
}
#endif

#endif

