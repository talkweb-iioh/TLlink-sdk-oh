/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "service_stdinc.h"
#include "tiot_state_api.h"

tiot_state_logcb_t g_logcb_ctx = NULL;

int32_t tiot_set_print_callback(tiot_state_logcb_t ctx)
{
    g_logcb_ctx = ctx;
    return 0;
}

