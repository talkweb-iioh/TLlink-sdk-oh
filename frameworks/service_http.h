/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _SERVICE_HTTP_H_
#define _SERVICE_HTTP_H_


#if defined(__cplusplus)
extern "C" {
#endif

#include "service_stdinc.h"
#include "tiot_state_api.h"
#include "tiot_sysdep_api.h"
#include "service_string.h"
#include "service_log.h"
#include "service_auth.h"
#include "tiot_http_api.h"

typedef enum {
    SERVICE_HTTP_SM_READ_HEADER,
    SERVICE_HTTP_SM_READ_BODY
} service_http_sm_t;

typedef struct {
    service_http_sm_t sm;
    uint32_t body_total_len;
    uint32_t body_read_len;
} service_http_session_t;

typedef struct {
    uint32_t code;
    uint8_t *content;
    uint32_t content_len;
    uint32_t content_total_len;
} service_http_response_t;


typedef struct {
    void *data_mutex;
    void *send_mutex;
    void *recv_mutex;
} service_http_mutex_t;

typedef struct {
    tiot_sysdep_portfile_t *sysdep;
    void *network_ctx;
    char *host;
    uint16_t port;
    char *product_key;
    char *device_name;
    char *device_secret;
    char *extend_devinfo;
    uint32_t connect_timeout_ms;
    uint32_t send_timeout_ms;
    uint32_t recv_timeout_ms;
    uint32_t auth_timeout_ms;
    uint32_t deinit_timeout_ms;
    uint32_t header_line_max_len;
    uint32_t body_buffer_max_len;
    tiot_sysdep_network_cred_t *cred;
    char *token;
    uint8_t long_connection;
    uint8_t exec_enabled;
    uint32_t exec_count;
    uint8_t service_exec_enabled;
    uint32_t service_exec_count;
    service_http_mutex_t all_mutex;
    service_http_session_t session;
    tiot_http_event_ctx_t event_ctx;
    tiot_http_recv_ctx_t recv_ctx;
    tiot_http_recv_ctx_t service_recv_ctx;
    void *userdata;
    void *service_userdata;
} service_http_ctx_t;

#define SERVICE_HTTP_DEINIT_INTERVAL_MS               (100)

#define SERVICE_HTTP_DEFAULT_CONNECT_TIMEOUT_MS       (10 * 1000)
#define SERVICE_HTTP_DEFAULT_AUTH_TIMEOUT_MS          (5 * 1000)
#define SERVICE_HTTP_DEFAULT_SEND_TIMEOUT_MS          (5 * 1000)
#define SERVICE_HTTP_DEFAULT_RECV_TIMEOUT_MS          (5 * 1000)
#define SERVICE_HTTP_DEFAULT_HEADER_LINE_MAX_LEN      (128)
#define SERVICE_HTTP_DEFAULT_BODY_MAX_LEN             (128)
#define SERVICE_HTTP_DEFAULT_DEINIT_TIMEOUT_MS        (2 * 1000)

typedef enum {
    SERVICE_HTTPOPT_HOST,
    SERVICE_HTTPOPT_PORT,
    SERVICE_HTTPOPT_NETWORK_CRED,
    SERVICE_HTTPOPT_CONNECT_TIMEOUT_MS,
    SERVICE_HTTPOPT_SEND_TIMEOUT_MS,
    SERVICE_HTTPOPT_RECV_TIMEOUT_MS,
    SERVICE_HTTPOPT_DEINIT_TIMEOUT_MS,
    SERVICE_HTTPOPT_HEADER_LINE_MAX_LEN,
    SERVICE_HTTPOPT_BODY_BUFFER_MAX_LEN,
    SERVICE_HTTPOPT_EVENT_HANDLER,
    SERVICE_HTTPOPT_USERDATA,
    SERVICE_HTTPOPT_RECV_HANDLER,
    SERVICE_HTTPOPT_MAX
} service_http_option_t;

typedef struct {
    char *method;
    char *path;
    char *header;
    uint8_t *content;
    uint32_t content_len;
} service_http_request_t;

void *service_http_init(void);
int32_t service_http_setopt(void *ctx, service_http_option_t option, void *data);
int32_t service_http_connect(void *ctx);
int32_t service_http_send(void *ctx, const service_http_request_t *request);
int32_t service_http_recv(void *ctx);
int32_t service_http_deinit(void **p_ctx);

#if defined(__cplusplus)
}
#endif

#endif /* #ifndef _service_http_H_ */

