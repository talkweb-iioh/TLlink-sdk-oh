/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _SERVICE_LIST_H_
#define _SERVICE_LIST_H_

#if defined(__cplusplus)
extern "C" {
#endif

#if ( defined(__ARMCC_VERSION) || defined(_MSC_VER) || defined(__GNUC__)) && \
    !defined(inline) && !defined(__cplusplus)
    #define inline __inline
#endif

struct service_list_head {
    struct service_list_head *prev;
    struct service_list_head *next;
};

/*
 * Get offset of a member variable.
 *
 * @param[in]   type     the type of the struct this is embedded in.
 * @param[in]   member   the name of the variable within the struct.
 */
#ifdef offsetof
    #undef offsetof
#endif
#define offsetof(type, member)   ((size_t)&(((type *)0)->member))

/*
 * Get the struct for this entry.
 *
 * @param[in]   ptr     the list head to take the element from.
 * @param[in]   type    the type of the struct this is embedded in.
 * @param[in]   member  the name of the variable within the struct.
 */
#define container_of(ptr, type, member) \
    ((type *) ((char *) (ptr) - offsetof(type, member)))

static inline void CORE_INIT_LIST_HEAD(struct service_list_head *list)
{
    list->next = list;
    list->prev = list;
}

/*
 * Insert a new entry between two known consecutive entries.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static inline void __service_list_add(struct service_list_head *node,
                              struct service_list_head *prev,
                              struct service_list_head *next)
{
    next->prev = node;
    node->next = next;
    node->prev = prev;
    prev->next = node;
}

/**
 * service_list_add - add a new entry
 * @node: new entry to be added
 * @head: list head to add it after
 *
 * Insert a new entry after the specified head.
 * This is good for implementing stacks.
 */
static inline void service_list_add(struct service_list_head *node, struct service_list_head *head)
{
    __service_list_add(node, head, head->next);
}

/**
 * service_list_add_tail - add a new entry
 * @node: new entry to be added
 * @head: list head to add it before
 *
 * Insert a new entry before the specified head.
 * This is useful for implementing queues.
 */
static inline void service_list_add_tail(struct service_list_head *node, struct service_list_head *head)
{
    __service_list_add(node, head->prev, head);
}

/*
 * Delete a list entry by making the prev/next entries
 * point to each other.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static inline void __service_list_del(struct service_list_head *prev, struct service_list_head *next)
{
    next->prev = prev;
    prev->next = next;
}

/**
 * service_list_del - deletes entry from list.
 * @entry: the element to delete from the list.
 * Note: list_empty() on entry does not return true after this, the entry is
 * in an undefined state.
 */
static inline void __service_list_del_entry(struct service_list_head *entry)
{
    __service_list_del(entry->prev, entry->next);
}

static inline void service_list_del(struct service_list_head *entry)
{
    __service_list_del_entry(entry);
    entry->next = entry;
    entry->prev = entry;
}

/**
 * service_list_empty - tests whether a list is empty
 * @head: the list to test.
 */
static inline int service_list_empty(const struct service_list_head *head)
{
    return head->next == head;
}

/**
 * service_list_entry - get the struct for this entry
 * @ptr:    the &struct service_list_head pointer.
 * @type:   the type of the struct this is embedded in.
 * @member: the name of the service_list_head within the struct.
 */
#define service_list_entry(ptr, type, member) \
    container_of(ptr, type, member)

/**
 * service_list_first_entry - get the first element from a list
 * @ptr:    the list head to take the element from.
 * @type:   the type of the struct this is embedded in.
 * @member: the name of the service_list_head within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define service_list_first_entry(ptr, type, member) \
    service_list_entry((ptr)->next, type, member)

/**
 * service_list_next_entry - get the next element in list
 * @pos:    the type * to cursor
 * @member: the name of the service_list_head within the struct.
 * @type:   the type of the struct this is embedded in
 */
#define service_list_next_entry(pos, member, type)         \
    service_list_entry((pos)->member.next, type, member)

/**
 * service_list_for_each_entry  -   iterate over list of given type
 * @pos:    the type * to use as a loop cursor.
 * @head:   the head for your list.
 * @member: the name of the service_list_head within the struct.
 * @type:   the type of the struct this is embedded in
 */
#define service_list_for_each_entry(pos, head, member, type)       \
    for (pos = service_list_first_entry(head, type, member);       \
         &pos->member != (head);                                \
         pos = service_list_next_entry(pos, member, type))

/**
 * service_list_for_each_entry_safe - iterate over list of given type safe against removal of list entry
 * @pos:    the type * to use as a loop cursor.
 * @n:      another type * to use as temporary storage
 * @head:   the head for your list.
 * @member: the name of the service_list_head within the struct.
 * @type:   the type of the struct this is embedded in
 */
#define service_list_for_each_entry_safe(pos, n, head, member, type)   \
    for (pos = service_list_first_entry(head, type, member),           \
         n = service_list_next_entry(pos, member, type);               \
         &pos->member != (head);                                    \
         pos = n, n = service_list_next_entry(n, member, type))

#if defined(__cplusplus)
}
#endif

#endif

