/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _SERVICE_SHA256_H_
#define _SERVICE_SHA256_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "service_stdinc.h"

#define SERVICE_SHA256_DIGEST_LENGTH            (32)
#define SERVICE_SHA256_BLOCK_LENGTH             (64)
#define SERVICE_SHA256_SHORT_BLOCK_LENGTH       (SERVICE_SHA256_BLOCK_LENGTH - 8)
#define SERVICE_SHA256_DIGEST_STRING_LENGTH     (SERVICE_SHA256_DIGEST_LENGTH * 2 + 1)

/**
 * \brief          SHA-256 context structure
 */
typedef struct {
    uint32_t total[2];          /*!< number of bytes processed  */
    uint32_t state[8];          /*!< intermediate digest state  */
    unsigned char buffer[64];   /*!< data block being processed */
    uint8_t is224;                  /*!< 0 => SHA-256, else SHA-224 */
} service_sha256_context_t;

/**
 * \brief          Initialize SHA-256 context
 *
 * \param ctx      SHA-256 context to be initialized
 */
void service_sha256_init(service_sha256_context_t *ctx);

/**
 * \brief          Clear SHA-256 context
 *
 * \param ctx      SHA-256 context to be cleared
 */
void service_sha256_free(service_sha256_context_t *ctx);


/**
 * \brief          SHA-256 context setup
 *
 * \param ctx      context to be initialized
 */
void service_sha256_starts(service_sha256_context_t *ctx);

/**
 * \brief          SHA-256 process buffer
 *
 * \param ctx      SHA-256 context
 * \param input    buffer holding the  data
 * \param ilen     length of the input data
 */
void service_sha256_update(service_sha256_context_t *ctx, const unsigned char *input, uint32_t ilen);

/**
 * \brief          SHA-256 final digest
 *
 * \param ctx      SHA-256 context
 * \param output   SHA-256 checksum result
 */
void service_sha256_finish(service_sha256_context_t *ctx, uint8_t output[32]);

/* Internal use */
void service_sha256_process(service_sha256_context_t *ctx, const unsigned char data[64]);

/**
 * \brief          Output = SHA-256( input buffer )
 *
 * \param input    buffer holding the  data
 * \param ilen     length of the input data
 * \param output   SHA-256 checksum result
 */
void service_sha256(const uint8_t *input, uint32_t ilen, uint8_t output[32]);

void service_hmac_sha256(const uint8_t *msg, uint32_t msg_len, const uint8_t *key, uint32_t key_len, uint8_t output[32]);

#if defined(__cplusplus)
}
#endif

#endif

