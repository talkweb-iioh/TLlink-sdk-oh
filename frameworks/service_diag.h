/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _SERVICE_DIAG_H_
#define _SERVICE_DIAG_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "service_stdinc.h"
#include "service_log.h"

typedef void (*service_diag_callback)(void * diag_ctx, uint64_t timestamp, int32_t code, uint8_t *data, uint32_t data_len);

void service_diag_set_cb(void *diag_ctx, service_diag_callback func);
void service_diag(tiot_sysdep_portfile_t *sysdep, int32_t code, uint8_t *data, uint32_t data_len);

#if defined(__cplusplus)
}
#endif

#endif
