/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "service_global.h"

typedef struct {
    void *mutex_lock;
    uint8_t init_id;
    uint32_t used_num;
    int32_t tlink_id;
    char backup_ip[16];
} g_service_global_t;

g_service_global_t g_service_global = {NULL, 0, 0, 0, {0}};

int32_t service_global_init(tiot_sysdep_portfile_t *sysdep_param)
{
    if(sysdep_param == NULL){
        return RET_USER_INPUT_BASE;
    }

    if (g_service_global.init_id == 1) {
        g_service_global.used_num++;
        return RET_SUCCESS;
    }
    g_service_global.init_id = 1;

    g_service_global.mutex_lock = sysdep_param->service_sysdep_mutex_init();
    g_service_global.used_num++;

    return RET_SUCCESS;
}

int32_t service_global_tlink_id_next(tiot_sysdep_portfile_t *sysdep_param, int32_t *tlink_id)
{
    if(sysdep_param == NULL){
        return RET_USER_INPUT_BASE;
    }
    int32_t id = 0;
    sysdep_param->service_sysdep_mutex_lock(g_service_global.mutex_lock);
    g_service_global.tlink_id++;
    if (g_service_global.tlink_id < 0) {
        g_service_global.tlink_id = 0;
    }
    id = g_service_global.tlink_id;
    sysdep_param->service_sysdep_mutex_unlock(g_service_global.mutex_lock);

    *tlink_id = id;
    return RET_SUCCESS;
}

int32_t service_global_set_backup_ip(tiot_sysdep_portfile_t *sysdep_param, char ip[16])
{
    if(sysdep_param == NULL){
        return RET_USER_INPUT_BASE;
    }

    sysdep_param->service_sysdep_mutex_lock(g_service_global.mutex_lock);
    sysdep_param->service_sysdep_memset(g_service_global.backup_ip, 0, 16);
    sysdep_param->service_sysdep_memcpy(g_service_global.backup_ip, ip, strlen(ip));
    sysdep_param->service_sysdep_mutex_unlock(g_service_global.mutex_lock);

    return RET_SUCCESS;
}

int32_t service_global_get_backup_ip(tiot_sysdep_portfile_t *sysdep_param, char ip[16])
{
    if(sysdep_param == NULL){
        return RET_USER_INPUT_BASE;
    }

    sysdep_param->service_sysdep_mutex_lock(g_service_global.mutex_lock);
    sysdep_param->service_sysdep_memcpy(ip, g_service_global.backup_ip, strlen(g_service_global.backup_ip));
    sysdep_param->service_sysdep_mutex_unlock(g_service_global.mutex_lock);

    return RET_SUCCESS;
}

int32_t service_global_deinit(tiot_sysdep_portfile_t *sysdep_param)
{
    if(sysdep_param == NULL){
        return RET_USER_INPUT_BASE;
    }

    if (g_service_global.used_num > 0) {
        g_service_global.used_num--;
    }

    if (g_service_global.used_num != 0) {
        return RET_SUCCESS;
    }
    sysdep_param->service_sysdep_mutex_deinit(&g_service_global.mutex_lock);

    g_service_global.mutex_lock = NULL;
    g_service_global.init_id = 0;
    g_service_global.used_num = 0;

    return RET_SUCCESS;
}

