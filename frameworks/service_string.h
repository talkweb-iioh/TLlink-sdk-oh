/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _SERVICE_STRING_H_
#define _SERVICE_STRING_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "service_stdinc.h"
#include "tiot_state_api.h"
#include "tiot_sysdep_api.h"

typedef struct {
    uint32_t year;
    uint32_t mon;
    uint32_t day;
    uint32_t hour;
    uint32_t min;
    uint32_t sec;
    uint32_t msec;
} service_date_t;

int32_t service_str2uint(char *input, uint8_t input_len, uint32_t *output);
int32_t service_str2uint64(char *input, uint8_t input_len, uint64_t *output);
int32_t service_uint2str(uint32_t input, char *output, uint8_t *output_len);
int32_t service_uint642str(uint64_t input, char *output, uint8_t *output_len);
int32_t service_int2str(int32_t input, char *output, uint8_t *output_len);
int32_t service_int2hexstr(int32_t input, char *output, uint8_t *output_len);
int32_t service_hex2str(uint8_t *input, uint32_t input_len, char *output, uint8_t lowercase);
int32_t service_str2hex(char *input, uint32_t input_len, uint8_t *output);
int32_t service_strdup(tiot_sysdep_portfile_t *sysdep, char **dest, char *src);
int32_t service_sprintf(tiot_sysdep_portfile_t *sysdep, char **dest, char *fmt, char *src[], uint8_t count);
int32_t service_json_value(tiot_sysdep_portfile_t *sysdep,const char *input, uint32_t input_len, const char *key, uint32_t key_len, char **value,
                        uint32_t *value_len);
int32_t service_utc2date(uint64_t utc, int8_t zone, service_date_t *date);
int32_t service_strcat(tiot_sysdep_portfile_t *sysdep, char **dest, char *src1, char *src2);

#if defined(__cplusplus)
}
#endif

#endif

