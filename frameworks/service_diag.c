/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "service_diag.h"

static void _service_diag_default_callback(void *diag_ctx, uint64_t timestamp, int32_t code, uint8_t *data, uint32_t data_len)
{

}

static void *g_diag_ctx = NULL;
static service_diag_callback g_diag_cb = _service_diag_default_callback;

void service_diag_set_cb(void *diag_ctx, service_diag_callback func)
{
    if(diag_ctx == NULL){
        return;
    }
    if (func != NULL) {
        g_diag_ctx = diag_ctx;
        g_diag_cb = func;
    }else{
        g_diag_cb = _service_diag_default_callback;
        g_diag_ctx = NULL;
    }
}

void service_diag(tiot_sysdep_portfile_t *sysdep, int32_t code, uint8_t *data, uint32_t data_len)
{
    g_diag_cb(g_diag_ctx, service_log_get_timestamp(sysdep), code, data, data_len);
}
