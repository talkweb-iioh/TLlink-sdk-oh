# 1. demo相关设置

sdk提供了几个demo，包括：`http_test.c mqtt_test.c ntp_test.c subdev_test.c`。他们对应的功能如下

| demo          | 功能           | 详细说明                                                     |
| ------------- | -------------- | ------------------------------------------------------------ |
| http_test.c   | 一型一密       | 同一产品下的设备烧录相同的设备标志信息（ProductKey和ProductSecret），这一产品如果获得认证建连三元组信息 |
| mqtt_test.c   | 发布与订阅功能 | ，这个demo包含了消息发布与订阅的功能，设备的事件上报、属性上报都是通过消息发布来实现的，云平台命令的下发则通过消息订阅来进行实现 |
| ntp_test.c    | 时钟同步       | 考虑到设备是受限的设备，设备本身没有时间信息，所以加入时钟同步模块，设备通过云端获取当前的时间信息 |
| subdev_test.c | 网关与子设备   | 网关与子设备模块，提供了拓扑结构添加以及删除、子设备上下线，子设备退网入网 |

快速上手可以直接跳转第二章节以及第三章节。然后第四章节则描述了相关的逻辑。



# 2.示例

## mqtt_test.c例子说明

### 修改并运行

整个项目通过makefile文件进行构建，最终将可执行文件生成在out文件夹下。其中`mqtt_test.c`则被生成可执行文件`test/mqtt_test.c`。

更改设备三元组以及host、port信息

```c
/* 设置host以及端口信息 */
char       *url = "116.63.137.223";
char        host[100] = {0};
uint16_t    port = 1883;

/* 设备的认证信息 */
char *product_key       = "r0SPgcPsc";
char *device_name       = "r0plgRlvR";
char *device_secret     = "a57cdfc800205846";
```

设置订阅消息的主题，`tiot_mqtt_sub(mq_process, sub_topic, NULL, 1, NULL)`为订阅消息的函数调用，`subtopic`为订阅的主题：

```c
/* mqtt订阅消息 */
{
    char *sub_topic = "persistent://T0001/default/r0SPgcPsc.r0plgRlvR.sys.event.propertyReport.post_reply";

    res = tiot_mqtt_sub(mq_process, sub_topic, NULL, 1, NULL);
    if (res < 0) {
        printf("tiot_mqtt_sub failed, res: -0x%04X\n", -res);
        return -1;
    }
}
```

设置发布消息的主题以及消息的内容，这里调用的发布消息的函数是` tiot_mqtt_pub_with_method(mq_process, pub_topic, (uint8_t *)pub_payload, (uint32_t)strlen(pub_payload), 0, "light")`

```
/* mqtt发布消息 */
{
    char *pub_topic = "persistent://T0001/default/r0SPgcPsc.r0plgRlvR.sys.event.propertyReport.post";
    char *pub_payload = "{\"LightSwitch\":0,\"height\":100}";

    res = tiot_mqtt_pub_with_method(mq_process, pub_topic, (uint8_t *)pub_payload, (uint32_t)strlen(pub_payload), 0, "light");
    if (res < 0) {
    	printf("tiot_mqtt_sub failed, res: -0x%04X\n", -res);
    	return -1;
    }
} 
```

运行程序：构建c工程+运行可执行文件

步骤一：在工程文件夹下构建工程`make`

步骤二：运行可执行文件`./out/mqtt-test`

![image-20211031140455959](png/运行步骤演示.png)

图中运行结果显示程序订阅了主题`persistent://T0001/default/r0SPgcPsc.r0plgRlvR.sys.event.propertyReport.post_reply`，同时也在主题`persistent://T0001/default/r0SPgcPsc.r0plgRlvR.sys.event.propertyReport.post`发布了内容，我们可以使用工具进行验证。用工具来接收发布的消息，工具订阅代码中所发布的主题，那么此时工具就会接收到到代码发布的消息（在实际中，工具代表的就是云平台或者其他的设备），与所写的代码进行交互，如下图所示：

工具中订阅了代码发布消息的主题，从图中可以看到工具实时收到了发布的消息。工具的使用见调试说明章节

![image-20211031172350268](png/工具交互-收到订阅的消息.png)



### 具体解释

#### 订阅与发布主题的设定规则：

设备属性、事件以及命令的相关设置主题有如下约定，只有约定好的主题才有相关权限进行发布或者订阅，==如果没有权限的主题被发布或者被订阅就会使连接中断==：

表中topic为固定字段+可变字段，其中固定字段为`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys`，下面表格表示的是可变字段：

|                    | 请求topic                                        | 响应topic                                               |
| ------------------ | ------------------------------------------------ | ------------------------------------------------------- |
| 上报属性           | .event.propertyReport.post                       | .event.propertyReport.post_reply                        |
| 设置设备属性       | .command.setProperty.exec                        | .command.setProperty.exec_reply                         |
| 获取设备属性       | .command.getProperty.exec                        | .command.getProperty.exec_reply                         |
| 设备事件（默认）   | .event.\${tsl.event.pid}.post                    | .event.\${tsl.event.pid}.post_reply                     |
| 设备事件（自定义） | .event.\${tsl.blockId}:${tsl.event.pid}.post     | .event.\${tsl.blockId}:\${tsl.event.pid}.post_reply     |
| 设备命令           | .command.\${tsl.command.pid}.exec                | .command.\${tsl.command.pid}.exec_reply                 |
| 设备命令（自定义） | .command.\${tsl.blockId}:${tsl.command.pid}.exec | .command.\${tsl.blockId}:\${tsl.command.pid}.exec_reply |

表中授权的topic在平台生成了设备信息之后同时也会被生成，也能够在平台中看到。登录进平台->设备实例->topic列表。

![image-20211030155653174](png/时钟同步topic.png)

通过平台我们也能够知道在代码中需要订阅或者发布哪些内容。平台地址：[Login - 登录 (talkweb.com.cn)](http://iotdemo.talkweb.com.cn/iot2.0/#/login) **（账户与密码需要联系产品对接人申请）**

#### 我们需要订阅或者哪些内容？

物模型是拓维物联网云平台为产品定义的数据模型。您可以通过配置C Link SDK，实现设备端上报属性和事件，并接收物联网平台发送的设置属性和命令下发的指令。

当设备需要进行上报属性的时候所进行发布以及订阅的主题？

订阅上报属性的请求topic以及云平台响应的topic，定义如上表所示，在实际使用中可以直接登录平台进行复制粘贴（同理事件的上报和命令的下发也是同样的操作）。如下所示：

![image-20211101112150988](png/上报属性topic复制.png)

平台已经为常见的设备提供了物模型设定，当设备厂商需要新增物模型定义或者平台没有对应的产品的时候，设备厂商就需要在平台进行新增物模型的操作。（操作步骤：进入平台——>进入产品——>topic类列表——>定义topic类）

![image-20211101103013128](png/自定义topic.png)

#### 设备对应的状态设定

**设备的在线**（不用进行相关设置，仅做功能的说明）

```c
/* 服务器认证 */
res = tiot_mqtt_connect(mq_process);
if (res < RET_SUCCESS) {
    /* 连接失败销毁资源 */
    tiot_mqtt_deinit(&mq_process);
    printf("tiot_mqtt_connect failed: -0x%04X\n", -res);
    return -1;
}
```

当服务器对设备三元组成功之后，此时就会在后台将设备的由未激活状态转为在线状态。

![image-20211030180340927](png/连接设备平台转为在线.png)

当一段时间设备不运行程序，也就是不发送心跳包消息的时候，此时平台该设备的状态将会改为离线状态，也会显示设备最后在线时间以便验证：

![image-20211031194827437](png/设备转为离线状态.png)



## ntp_test.c例子说明

ntp主要是用于受限的设备中，在很多受限的时候并没有准确的时间，而云平台又需要保证时间在五分钟之内，所以sdk通过ntp来保证受限的设备也能够获得时间值。

### 修改并运行

整个项目通过makefile文件进行构建，最终将可执行文件生成在out文件夹下。其中`ntp_test.c`则被生成可执行文件`test/ntp_test.c`。

更改设备三元组以及host、port信息

```c
/* 设置host以及端口信息 */
char       *url = "116.63.137.223";
char        host[100] = {0};
uint16_t    port = 1883;

/* 设备的认证信息 */
char *product_key       = "r0SPgcPsc";
char *device_name       = "r0plgRlvR";
char *device_secret     = "a57cdfc800205846";
```

设置订阅消息的主题`tiot_mqtt_sub(mq_process, sub_topic, NULL, 1, NULL)`：

```c
/* 对ntp的服务订阅，获取时间同步消息 */
{
    char *sub_topic = "persistent://T0001/default/r0SPgcPsc.r0plgRlvR.ext.ntp_reply";

    res = tiot_mqtt_sub(mq_process, sub_topic, NULL, 1, NULL);
    if (res < 0) {
        printf("tiot_mqtt_sub failed, res: -0x%04X\n", -res);
        return -1;
    }
}
```

设置发布消息的主题以及消息的内容` tiot_mqtt_pub_with_method(mq_process, pub_topic, (uint8_t *)pub_payload, (uint32_t)strlen(pub_payload), 0, "light")`

```c
 /* 对ntp的请求发送 */
 {
     //TODO:主题待设定
     char *pub_topic = "persistent://T0001/default/r0SPgcPsc.r0plgRlvR.ext.ntp";//


    char *pub_payload ;

    //将数据写入到json，然后写入到字符串
    uint64_t time=service_log_get_timestamp(((service_mq_process_t *) mq_process)->sysdep);
    cJSON *time_json= cJSON_CreateNumber(time);
    cJSON *pub_payload_json=cJSON_CreateObject();
    cJSON_AddItemToObject(pub_payload_json,"deviceSendTime",time_json);
    pub_payload=cJSON_Print(pub_payload_json);


    res = tiot_mqtt_pub_with_method(mq_process, pub_topic, (uint8_t *)pub_payload, (uint32_t)strlen(pub_payload), 0, "ext.ntp");
    if (res < 0) {
    	printf("tiot_mqtt_sub failed, res: -0x%04X\n", -res);
    	return -1;
    }
} 
```

运行程序：构建c文件+运行可执行文件

步骤一：构建工程`make`

步骤二：运行可执行文件`./out/ntp-test`

![image-20211101203017299](png/调试示例/ntp运行结果.png)

图中运行结果显示程序订阅了主题`persistent://T0001/default/r0SPgcPsc.r0plgRlvR.ext.ntp_reply`，同时也在主题`persistent://T0001/default/r0SPgcPsc.r0plgRlvR.ext.ntp`发布了内容，通过接收到的data字段可以取得当前时间，由图可见程序最终计算得到的时间是1635769544981。时间的计算公式为`(${serverRecvTime}+${serverSendTime}+${deviceRecvTime}-${deviceSendTime})/2`

我们也可以使用工具订阅代码发布消息的主题，从图中可以看到工具实时收到了发布的消息。工具的使用见调试说明章节

![image-20211101161736109](png/工具-ntp收到的消息显示.png)

## subdev_test.c例子说明

### 修改并运行

整个项目通过makefile文件进行构建，最终将可执行文件生成在out文件夹下。其中`subdev_test.c`则被生成可执行文件`test/subdev_test.c`。

更改设备三元组以及host、port信息

```c
/* 设置host以及端口信息 */
char       *url = "116.63.137.223";
char        host[100] = {0};
uint16_t    port = 1883;

/* 设备的认证信息 */
char *product_key       = "r0SPgcPsc";
char *device_name       = "r0plgRlvR";
char *device_secret     = "a57cdfc800205846";
```

程序中动态注册、拓扑添加、子设备上下线等分为不同得程序的程序块，当需要使用某一块功能就将注释去掉。比如动态注册如下：

需要替换对应的topic

```c
/* mqtt订阅MQTT动态注册响应消息 */
    {
        char *sub_topic = "persistent://T0001/default/8725dfbe93a64c5db1653ca766c036ce.8e39687812a643b597bf7309e43b41d3.sys.sub.register_reply";

        res = tiot_mqtt_sub(mqtt_handle, sub_topic, NULL, 1, NULL);
        if (res < 0)
        {
            printf("tiot_mqtt_sub failed, res: -0x%04X\n", -res);
            return -1;
        }
    }

    /* mqtt发布MQTT动态注册消息 */
    {
        char *pub_topic = "persistent://T0001/default/8725dfbe93a64c5db1653ca766c036ce.8e39687812a643b597bf7309e43b41d3.sys.sub.register";
        /* 修改成子设备的相关信息 */
        char *pub_payload = "{\"deviceName\":\"gate_child_device01\",\"productKey\":\"M0eGgpGpp\"}";

            res = tiot_mqtt_pub_with_method(mqtt_handle, pub_topic, (uint8_t *)pub_payload, (uint32_t)strlen(pub_payload), 0, "sub.register");
            if (res < 0)
            {
                printf("tiot_mqtt_sub failed, res: -0x%04X\n", -res);
                return -1;
            }
    }
```

#### 运行结果

以下是子设备动态注册的输出：

![image-20211020203156058](png/动态注册运行结果.png)

## http_test.c例子

这个例子主要用于一型一密的认证，在一型一密认证方式下，同一产品下所有设备可以烧录相同的设备标志信息，即所有设备包含相同的产品证书（ProductKey和ProductSecret）。设备发送激活请求时，物联网平台会进行身份确认，认证通过后，下发设备接入所需信息。一型一密最终得到的是设备的三元组信息。

一型一密的情况下采用http通道进行请求，这个时候用户只需要关注以下几个部分。

1. host的设置

   ```c
   char  *http_host = "116.63.137.223";
   ```

2. 三元组的设置

   ```c
   char *device_secret       = "5da4d39b8030c6f0";//产品的密钥
   char *device_name       = "2";//设备的唯一标识
   char *product_key     = "r0SPgcPsc";//产品名字
   ```

当设置了这些信息之后就可以运行程序`out/http_test.c`，我们可以看到得到的设备三元组信息，如下所示：

![image-20211030175745584](png/http一型一密三元组结果.png)

得到了设备的三元组信息之后，我们就可以利用这个三元组信息与平台进行连接。

# 3. 调试说明

## 验证软件的使用

验证软件的使用：

软件下载：[MQTT X - 优雅的跨平台 MQTT 5.0 桌面客户端工具](https://mqttx.app/zh)

1.对用户名以及密码等进行设置

![img](png/调试示例/用户名以及密码等设置.png)

2.这里有三个变量是需要进行专门设置，name只需要做到唯一（也就是可随意设置，注意与已有的不要冲突），host与port参考sdk

![img](png/调试示例/mqttx三个变量设置1.jpg)

![img](png/调试示例/mqttx三个变量设置2.jpg)

**变量一：clientid**

由以下组成（这里的+表示连接符号，实际测试程序中不用双引号）：

clientId+"|signmethod=hmacmd5,timestamp=0|"

对应程序中：123459|signmethod=hmacmd5,timestamp=0|

对应关系：

1. clientId可随意设置，需保证唯一性；

2. signmethod表示签名算法，指定用何种哈希密钥算法生成密码，示例中指定HmacMD5；

3. timestamp表示时间戳，需要保证在五分钟以内，同时这里的时间戳需要是毫秒级。注意为了方便调试我们将时间戳设置为0（[时间戳(Unix timestamp)转换工具 - 在线工具 (tool.lu)](https://tool.lu/timestamp/)）

 

**变量二：Username**

由以下组成（这里的+表示连接符号，实际测试程序中不用双引号）：设备名称以及产品名组成

deviceName+"&"+productKey

对应程序中：8e39687812a643b597bf7309e43b41d3&8725dfbe93a64c5db1653ca766c036ce

对应变量含义：

1. deviceName表示设备的名称，对应的就是在设置三元组的时候三个变量之一

2. productKey表示产品的名称，对应的就是在设置三元组的时候三个变量之一

![img](png/调试示例/username生成.png)

**变量三：Password**
 由以下组成（这里的+表示连接符号，实际测试程序中不用双引号）：设备名称以及产品名组成

mqttPassword=hmacmd5("devicesecret","clientId12345deviceNamedevice1productKeypktimestamp0")

对应程序中：3f7b04f6497f9c13cfad8e25999bf903

变量生成和工具使用（[散列加密 | 哈希加密 |哈希解密 | 散列解密 (sojson.com)](https://www.sojson.com/hash.html)）：

 

![img](png/调试示例/password生成.png)

将图中的clientid（clientid自己设置需要做到与已有的不一样），devicename、productkey、时间戳、deviceSecret进行一一设置。

需要进行hash的值：

clientId123459deviceName8e39687812a643b597bf7309e43b41d3productKey8725dfbe93a64c5db1653ca766c036cetimestamp0

秘钥（注意这个秘钥就对应sdk中三元组的device_secret）：

3f7b04f6497f9c13cfad8e25999bf903

![img](png/调试示例/三元组对应设置.jpg)

最终结果生成的就是密码

其他变量：

1. name：随意起一个，需做到唯一

2. host：对应sdk中host

3. port：对应sdk中port

### 举例说明：

![img](png/举例说明.jpg)

 

1. Test

2. 45|signmethod=hmacsha256,timestamp=0|

3. mqtt://

4. 116.63.137.223

5. 1883

6. 8e39687812a643b597bf7309e43b41d3&8725dfbe93a64c5db1653ca766c036ce

7. 83259a55665eef493f9cd54dfb0f2ccd046b1493aaf1adb5ceb6638ac0094bb3

## 调试使用

如下图所示：

![image-20211031095353212](png/connect界面.png)



连接界面分为三个区域：订阅、发布、参数设备、状态显示

![image-20211031112522858](png/界面内容分布.png)

**调试步骤**：

使用mqttx工具进行联调。

当订阅了相关主题之后就可以在mqttx进行对指定主题上消息的发布。

1.mqttx在指定主题上发布消息

![img](png/调试示例/mqttx在指定topic发布消息.png)

2.在sdk中就会接收到相关消息并打印

![image-20211101150456768](png/调试示例/sdk接收到消息.png)

同理发布消息也只需要订阅主题，那么当设备运行sdk发布消息的时候，mqttx也会接收并打印相关消息。

# 4.demo之间的相同以及不同之处

## 相同的地方

无论是时钟同步、网关子设备以及发布与订阅功能实现，在对这些功能的调用之前都需要对建连的认证消息进行设备，这些认证消息包括三元组设备以及clientid的设置。

### 三元组的设置

```c
/* host以及端口的设置 */
char       *url = "116.63.137.223";
char        host[100] = {0};
uint16_t    port = 1883;

/* TODO: 替换为您设备的认证信息 */
char *product_key       = "r0SPgcPsc";
char *device_name       = "r0plgRlvR";
char *device_secret     = "a57cdfc800205846";
```

### clientid的设置

云平台会检测每一次连接客户端的clientid，这里的clientid需要做到唯一。clientid由`adapter/posix_port.c`中进行设置，sdk默认了一个返回值比如下面的“talkweb001”，

```c
char* service_unique_identifier(void)
{
    return "talkweb001";
}
```



## 不同的地方

### topic说明

要想实现不同的功能需要对特定的topic进行订阅或者发布。比如需要进行时钟同步的时候，就要对`persistent://${tenantid}/${user}/${productKey}.${deviceName}.ext.ntp`发送请求消息，同时订阅`persistent://${tenantid}/${user}/${productKey}.${deviceName}.ext.ntp_reply`，对接收到的数据进行提取获得当前的时间。这些topic在tlink协议中都有相关指定。如下图所示：

![image-20211030150629918](png/tlink中功能与topic权限对应关系.png)

对主题的订阅，在对应的设备只有特定的topic才有发布与接收的权限，这些在tlink协议中具有定义。

![image-20211030144636414](png/topic权限说明.png)

topic中字段的定义，以`persistent://${tenantid}/${user}/${productKey}.${deviceName}.ext.ntp`为例，字段意义如下所示：

1. persistent为固定字段；
2. ${tenantid}为设备厂商或开发者用户的租户id，可以试注册账号的所属公司、组织的标识；
3. ${user}为创建产品、设备的用户唯一标识，目前取值**default**，暂时不对用户分配
4. ${productKey}为产品名称；
5. ${deviceName}为设备名称；

设备在平台创建之后就会生成对应topic的权限，这时候就可以在平台中进行查看：

![image-20211030155653174](png/时钟同步topic.png)

![image-20211030165645805](png/topic权限说明-物模型.png)

![image-20211030170422536](png/topic权限说明-自定义.png)

在这几个demo中除了http_test.c都是通过在指定的topic发送对应的内容请求来实现相关功能，

### 一型一密

一型一密认证方式下，同一产品下所有设备可以烧录相同的设备标志信息，即所有设备包含相同的产品证书（ProductKey和ProductSecret）。设备发送激活请求时，物联网平台会进行身份确认，认证通过后，下发设备接入所需信息。一型一密最终得到的是设备的三元组信息。

一型一密的情况下采用http通道进行请求，这个时候用户只需要关注以下几个部分。

1. host的设置

   ```c
   char  *http_host = "116.63.137.223";
   ```

2. 三元组的设置

   ```c
   char *device_secret       = "5da4d39b8030c6f0";//产品的密钥
   char *device_name       = "2";//设备的唯一标识
   char *product_key     = "r0SPgcPsc";//产品名字
   ```

当设置了这些信息之后就可以运行程序，我们可以看到得到的设备三元组信息，如下所示：

![image-20211030175745584](png/http一型一密三元组结果.png)

得到了设备的三元组信息之后，我们就可以利用这个三元组信息与平台进行连接。