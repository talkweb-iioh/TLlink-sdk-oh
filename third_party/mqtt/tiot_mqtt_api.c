/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <stdio.h>
#include "tiot_mqtt.h"
#include "cJSON.h"

static int32_t _service_mqtt_sysdep_return(int32_t sysdep_code, int32_t service_code)
{
    if (sysdep_code >= (RET_PORT_BASE - 0x00FF) && sysdep_code < (RET_PORT_BASE)) {
        return sysdep_code;
    } else {
        return service_code;
    }
}

static int32_t _service_mqtt_5_feature_is_enabled(service_mq_process_t *mq_process)
{
    return (TIOT_MQTT_VERSION_5_0 == mq_process->protocol_version);
}

static void _service_mqtt_event_notify_process_ctx(service_mq_process_t *mq_process, tiot_mqtt_event_t *event,
        service_mqtt_event_t *service_event)
{
    service_mqtt_process_data_node_t *node = NULL;

    service_list_for_each_entry(node, &mq_process->process_data_list,
                             linked_node, service_mqtt_process_data_node_t) {
        node->process_data.ctx(node->process_data.context, event, service_event);
    }
}

static void _service_mqtt_event_notify(service_mq_process_t *mq_process, tiot_mqtt_event_type_t type)
{
    tiot_mqtt_event_t event;
    memset(&event, 0, sizeof(tiot_mqtt_event_t));
    event.type = type;

    if (mq_process->event_ctx) {
        mq_process->event_ctx((void *)mq_process, &event, mq_process->userdata);
    }

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.process_ctx_mutex);
    _service_mqtt_event_notify_process_ctx(mq_process, &event, NULL);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.process_ctx_mutex);
}

static void _service_mqtt_connect_event_notify(service_mq_process_t *mq_process)
{
    mq_process->cn_flag.disconn = 0;
    if(mq_process->cn_flag.tmp_conn != 0) {
        _service_mqtt_event_notify(mq_process, TIOT_MQTT_EVENT_RECONNECT);
    }else {
        mq_process->cn_flag.tmp_conn = 1;
        _service_mqtt_event_notify(mq_process, TIOT_MQTT_EVENT_CONNECT);
    }
}

static void _service_mqtt_disconnect_event_notify(service_mq_process_t *mq_process,
        tiot_mqtt_disconnect_event_type_t disconnect)
{
    if (mq_process->cn_flag.tmp_conn == 1 && mq_process->cn_flag.disconn == 0) {
        tiot_mqtt_event_t event;

        mq_process->cn_flag.disconn = 1;

        memset(&event, 0, sizeof(tiot_mqtt_event_t));
        event.type = TIOT_MQTT_EVENT_DISCONNECT;
        event.data.disconnect = disconnect;

        if (mq_process->event_ctx) {
            mq_process->event_ctx((void *)mq_process, &event, mq_process->userdata);
        }

        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.process_ctx_mutex);
        _service_mqtt_event_notify_process_ctx(mq_process, &event, NULL);
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.process_ctx_mutex);
    }
}

static void _service_mqtt_exec_inc(service_mq_process_t *mq_process)
{
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    mq_process->exec_count++;
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
}

static void _service_mqtt_exec_dec(service_mq_process_t *mq_process)
{
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    mq_process->exec_count--;
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
}

static void _service_mqtt_sign_clean(service_mq_process_t *mq_process)
{
    if (mq_process->username) {
        mq_process->sysdep->service_sysdep_free(mq_process->username);
        mq_process->username = NULL;
    }
    if (mq_process->password) {
        mq_process->sysdep->service_sysdep_free(mq_process->password);
        mq_process->password = NULL;
    }
    if (mq_process->clientid) {
        mq_process->sysdep->service_sysdep_free(mq_process->clientid);
        mq_process->clientid = NULL;
    }
}

static int32_t _service_mq_processrlist_insert(service_mq_process_t *mq_process, service_mqtt_sub_node_t *sub_node,
        tiot_mqtt_recv_ctx_t ctx, void *userdata)
{
    service_mqtt_sub_ctx_node_t *node = NULL;

    service_list_for_each_entry(node, &sub_node->ctx_list, linked_node, service_mqtt_sub_ctx_node_t) {
        if (node->ctx == ctx) {
            /* exist ctx, replace userdata */
            node->userdata = userdata;
            return RET_SUCCESS;
        }
    }

    if (&node->linked_node == &sub_node->ctx_list) {
        /* new ctx */
        node = mq_process->sysdep->service_sysdep_malloc(sizeof(service_mqtt_sub_ctx_node_t));
        if (node == NULL) {
            return RET_SYS_DEPEND_MALLOC_FAILED;
        }
        memset(node, 0, sizeof(service_mqtt_sub_ctx_node_t));
        CORE_INIT_LIST_HEAD(&node->linked_node);
        node->ctx = ctx;
        node->userdata = userdata;

        service_list_add_tail(&node->linked_node, &sub_node->ctx_list);
    }

    return RET_SUCCESS;
}

static int32_t _service_mqtt_sublist_insert(service_mq_process_t *mq_process, service_mqtt_buff_t *topic,
        tiot_mqtt_recv_ctx_t ctx, void *userdata)
{
    int32_t res = RET_SUCCESS;
    service_mqtt_sub_node_t *node = NULL;

    service_list_for_each_entry(node, &mq_process->sub_list, linked_node, service_mqtt_sub_node_t) {
        if ((strlen(node->topic) == topic->len) && memcmp(node->topic, topic->buffer, topic->len) == 0) {
            /* exist topic */
            if (ctx != NULL) {
                return _service_mq_processrlist_insert(mq_process, node, ctx, userdata);
            } else {
                return RET_SUCCESS;
            }
        }
    }

    if (&node->linked_node == &mq_process->sub_list) {
        /* new topic */
        node = mq_process->sysdep->service_sysdep_malloc(sizeof(service_mqtt_sub_node_t));
        if (node == NULL) {
            return RET_SYS_DEPEND_MALLOC_FAILED;
        }
        memset(node, 0, sizeof(service_mqtt_sub_node_t));
        CORE_INIT_LIST_HEAD(&node->linked_node);
        CORE_INIT_LIST_HEAD(&node->ctx_list);

        node->topic = mq_process->sysdep->service_sysdep_malloc(topic->len + 1);
        if (node->topic == NULL) {
            mq_process->sysdep->service_sysdep_free(node);
            return RET_SYS_DEPEND_MALLOC_FAILED;
        }
        memset(node->topic, 0, topic->len + 1);
        memcpy(node->topic, topic->buffer, topic->len);

        if (ctx != NULL) {
            res = _service_mq_processrlist_insert(mq_process, node, ctx, userdata);
            if (res < RET_SUCCESS) {
                mq_process->sysdep->service_sysdep_free(node->topic);
                mq_process->sysdep->service_sysdep_free(node);
                return res;
            }
        }

        service_list_add_tail(&node->linked_node, &mq_process->sub_list);
    }
    return res;
}

static int32_t _service_mqtt_topic_alias_list_insert(service_mq_process_t *mq_process, service_mqtt_buff_t *topic,
        uint16_t topic_alias, struct service_list_head *list)
{
    int32_t res = RET_SUCCESS;
    service_mqtt_topic_alias_node_t *node = NULL;

    service_list_for_each_entry(node, list, linked_node, service_mqtt_topic_alias_node_t) {
        if ((strlen(node->topic) == topic->len) && memcmp(node->topic, topic->buffer, topic->len) == 0) {
            /* exist topic */
            return RET_SUCCESS;
        }
    }

    if (&node->linked_node == list) {
        /* new topic */
        node = mq_process->sysdep->service_sysdep_malloc(sizeof(service_mqtt_topic_alias_node_t));
        if (node == NULL) {
            return RET_SYS_DEPEND_MALLOC_FAILED;
        }
        memset(node, 0, sizeof(service_mqtt_topic_alias_node_t));
        CORE_INIT_LIST_HEAD(&node->linked_node);

        node->topic = mq_process->sysdep->service_sysdep_malloc(topic->len + 1);
        if (node->topic == NULL) {
            mq_process->sysdep->service_sysdep_free(node);
            return RET_SYS_DEPEND_MALLOC_FAILED;
        }
        memset(node->topic, 0, topic->len + 1);
        memcpy(node->topic, topic->buffer, topic->len);
        node->topic_alias = topic_alias;

        service_list_add_tail(&node->linked_node, list);
    }
    return res;
}

static void _service_mqtt_topic_alias_list_remove_all(service_mq_process_t *mq_process)
{
    service_mqtt_topic_alias_node_t *node = NULL, *next = NULL;
    service_list_for_each_entry_safe(node, next, &mq_process->rx_topic_alias_list, linked_node,
                                  service_mqtt_topic_alias_node_t) {
        service_list_del(&node->linked_node);
        mq_process->sysdep->service_sysdep_free(node->topic);
        mq_process->sysdep->service_sysdep_free(node);
    }

    service_list_for_each_entry_safe(node, next, &mq_process->tx_topic_alias_list, linked_node,
                                  service_mqtt_topic_alias_node_t) {
        service_list_del(&node->linked_node);
        mq_process->sysdep->service_sysdep_free(node->topic);
        mq_process->sysdep->service_sysdep_free(node);
    }
}

static void _service_mqtt_sublist_ctxlist_destroy(service_mq_process_t *mq_process, struct service_list_head *list)
{
    service_mqtt_sub_ctx_node_t *node = NULL, *next = NULL;

    service_list_for_each_entry_safe(node, next, list, linked_node, service_mqtt_sub_ctx_node_t) {
        service_list_del(&node->linked_node);
        mq_process->sysdep->service_sysdep_free(node);
    }
}

static void _service_mqtt_sublist_remove(service_mq_process_t *mq_process, service_mqtt_buff_t *topic)
{
    if(mq_process == NULL || topic == NULL){
        return;
    }
    service_mqtt_sub_node_t *node = NULL;
    service_mqtt_sub_node_t *next = NULL;

    service_list_for_each_entry_safe(node, next, &mq_process->sub_list, linked_node, service_mqtt_sub_node_t) {
        if ((strlen(node->topic) == topic->len) && memcmp(node->topic, topic->buffer, topic->len) == 0) {
            service_list_del(&node->linked_node);
            _service_mqtt_sublist_ctxlist_destroy(mq_process, &node->ctx_list);
            mq_process->sysdep->service_sysdep_free(node->topic);
            mq_process->sysdep->service_sysdep_free(node);
        }
    }
}

static void _service_rm_mqtt_sublist(service_mq_process_t *mq_process, service_mqtt_buff_t *topic,
        tiot_mqtt_recv_ctx_t ctx)
{
    service_mqtt_sub_node_t *node = NULL;
    service_mqtt_sub_ctx_node_t *ctx_node = NULL, *ctx_next = NULL;

    service_list_for_each_entry(node, &mq_process->sub_list, linked_node, service_mqtt_sub_node_t) {
        if ((strlen(node->topic) == topic->len) && memcmp(node->topic, topic->buffer, topic->len) == 0) {
            service_list_for_each_entry_safe(ctx_node, ctx_next, &node->ctx_list,
                                          linked_node, service_mqtt_sub_ctx_node_t) {
                if (ctx_node->ctx == ctx) {
                    service_list_del(&ctx_node->linked_node);
                    mq_process->sysdep->service_sysdep_free(ctx_node);
                }
            }
        }
    }
}

static void _service_mqtt_sublist_destroy(service_mq_process_t *mq_process)
{
    service_mqtt_sub_node_t *node = NULL;
    service_mqtt_sub_node_t *next = NULL;

    service_list_for_each_entry_safe(node, next, &mq_process->sub_list, linked_node, service_mqtt_sub_node_t) {
        service_list_del(&node->linked_node);
        _service_mqtt_sublist_ctxlist_destroy(mq_process, &node->ctx_list);
        mq_process->sysdep->service_sysdep_free(node->topic);
        mq_process->sysdep->service_sysdep_free(node);
    }
}

static int32_t _service_mqtt_topic_is_valid(char *topic, uint32_t len)
{
    uint32_t idx = 0;

    for (idx = 0; idx < len; idx++) {
        if (topic[idx] == '+') {
            if ((topic[idx - 1] != '/') ||
                ((idx + 1 < len) && (topic[idx + 1] != '/'))) {
                return RET_MQTT_TOPIC_INVALID;
            }
        }
        if (topic[idx] == '#') {
            if ((topic[idx - 1] != '/') ||
                (idx + 1 < len)) {
                return RET_MQTT_TOPIC_INVALID;
            }
        }
    }

    return RET_SUCCESS;
}

static int32_t _service_mqtt_append_topic_map(service_mq_process_t *mq_process, tiot_mqtt_topic_map_t *map)
{
    int32_t             res = RET_SUCCESS;
    service_mqtt_buff_t    topic_buff;

    if (map->topic == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (strlen(map->topic) >= SERVICE_MQTT_TOPIC_MAXLEN) {
        return RET_MQTT_TOPIC_TOO_LONG;
    }
    if ((res = _service_mqtt_topic_is_valid((char *)map->topic, strlen((char *)map->topic))) < RET_SUCCESS) {
        return res;
    }

    memset(&topic_buff, 0, sizeof(topic_buff));
    topic_buff.buffer = (uint8_t *)map->topic;
    topic_buff.len = strlen(map->topic);

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.sub_mutex);
    res = _service_mqtt_sublist_insert(mq_process, &topic_buff, map->ctx, map->userdata);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.sub_mutex);

    return res;
}

static int32_t _service_mqtt_remove_topic_map(service_mq_process_t *mq_process, tiot_mqtt_topic_map_t *map)
{
    int32_t             res = RET_SUCCESS;
    service_mqtt_buff_t    topic_buff;

    if (map->topic == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (strlen(map->topic) >= SERVICE_MQTT_TOPIC_MAXLEN) {
        return RET_MQTT_TOPIC_TOO_LONG;
    }
    if ((res = _service_mqtt_topic_is_valid((char *)map->topic, strlen((char *)map->topic))) < RET_SUCCESS) {
        return res;
    }

    memset(&topic_buff, 0, sizeof(topic_buff));
    topic_buff.buffer = (uint8_t *)map->topic;
    topic_buff.len = strlen(map->topic);

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.sub_mutex);
    _service_rm_mqtt_sublist(mq_process, &topic_buff, map->ctx);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.sub_mutex);

    return RET_SUCCESS;
}

static void _service_mqtt_set_utf8_encoded_str(uint8_t *input, uint16_t input_len, uint8_t *output)
{
    uint32_t idx = 0, input_idx = 0;

    /* String Length MSB */
    output[idx++] = (uint8_t)((input_len >> 8) & 0x00FF);

    /* String Length LSB */
    output[idx++] = (uint8_t)((input_len) & 0x00FF);

    /* UTF-8 Encoded Character Data */
    for (input_idx = 0; input_idx < input_len; input_idx++) {
        output[idx++] = input[input_idx];
    }
}

static void _service_mqtt_remain_len_encode(uint32_t input, uint8_t *output, uint32_t *output_idx)
{
    uint8_t encoded_byte = 0, idx = 0;

    do {
        encoded_byte = input % 128;
        input /= 128;
        if (input > 0) {
            encoded_byte |= 128;
        }
        output[idx++] = encoded_byte;
    } while (input > 0);

    *output_idx += idx;
}

static int32_t _service_mqtt_general_property_is_valid(general_property_t *general_property)
{
    uint32_t idx = 0;

    if (NULL == general_property) {
        return RET_SUCCESS;
    }

    /* User Property */
    mqtt5_property_t **user_prop_list = general_property->user_property;
    do {
        mqtt5_property_t *user_prop = user_prop_list[idx++];
        if (NULL != user_prop) {
            if (user_prop->key.len > SERVICE_MQTT_USER_PROPERTY_KEY_MAX_LEN
                || user_prop->value.len > SERVICE_MQTT_USER_PROPERTY_VALUE_MAX_LEN
               ) {
                return RET_MQTT_INVALID_USER_PERPERTY_LEN;
            }
            if ((user_prop->key.len > 0 && NULL == user_prop->key.value)
                || (user_prop->value.len > 0 && NULL == user_prop->value.value)) {
                return RET_MQTT_INVALID_USER_PERPERTY_DATA;
            }
        }
    } while (idx < MQTT5_ATTRIBUTE_MAX);

    /* Subscription Identifier */
    if (general_property->subscription_identifier > 0xFFFFFFF) {
        return RET_MQTT_INVALID_SUBSCRIPTION_IDENTIFIER;
    }

    return RET_SUCCESS;
}

uint32_t _service_get_general_property_len(general_property_t *general_prop);
void _service_write_general_prop(uint8_t *pos, uint32_t *index, general_property_t *general_property,
                              uint8_t *property_len_array, uint8_t property_len_offset)
{
    uint32_t idx = *index;
    uint16_t topic_alias_max = general_property->topic_alias_max;
    uint16_t topic_alias = general_property->topic_alias;
    uint16_t client_receive_max = general_property->client_receive_max;
    mqtt5_property_element_t *reason_string = general_property->reason_string;
    mqtt5_property_t **prop_list = general_property->user_property;

    /* Total Length of All Properties */
    if (NULL != property_len_array) {
        memcpy(&(pos[idx]), property_len_array, property_len_offset);
        idx += property_len_offset;
    }

    /*User Properties */
    if (NULL != prop_list) {
        int iter = 0;
        for (iter = 0; iter < MQTT5_ATTRIBUTE_MAX; iter++) {
            mqtt5_property_t *prop = prop_list[iter];
            if (NULL != prop) {
                pos[idx++] = SERVICE_MQTTPROP_USER_PROPERTY;
                pos[idx++] = prop->key.len << 8;
                pos[idx++] = prop->key.len & 0xff;
                memcpy(&(pos[idx]), prop->key.value, prop->key.len);
                idx += prop->key.len;

                pos[idx++] = prop->value.len << 8;
                pos[idx++] = prop->value.len & 0xff;
                memcpy(&(pos[idx]), prop->value.value, prop->value.len);
                idx += prop->value.len;
            }
        }
    }

    /*User Topic talkweb max */
    if (topic_alias_max > 0) {
        pos[idx++] = SERVICE_MQTTPROP_TOPIC_ALIAS_MAX;
        pos[idx++] = topic_alias_max >> 8;
        pos[idx++] = topic_alias_max & 0xFF;
    }

    /* Topic talkweb */
    if (topic_alias > 0) {
        pos[idx++] = SERVICE_MQTTPROP_TOPIC_ALIAS;
        pos[idx++] = topic_alias >> 8;
        pos[idx++] = topic_alias & 0xFF;
    }

    /*Client Receive Max */
    if (client_receive_max > 0) {
        pos[idx++] = SERVICE_MQTTPROP_RECEIVE_MAXIMUM;
        pos[idx++] = client_receive_max >> 8;
        pos[idx++] = client_receive_max & 0xFF;
    }

    /* Response Topic */
    if (0 != general_property->response_topic.len) {
        pos[idx++] = SERVICE_MQTTPROP_RESPONSE_TOPIC;
        uint16_t topic_len = general_property->response_topic.len;
        pos[idx++] = topic_len >> 8;
        pos[idx++] = topic_len & 0xFF;
        memcpy(&(pos[idx]), general_property->response_topic.value, topic_len);
        idx += topic_len;
    }

    /* Correlation Data */
    if (0 != general_property->correlation_data.len) {
        pos[idx++] = SERVICE_MQTTPROP_CORRELATION_DATA;
        uint16_t data_len = general_property->correlation_data.len;
        pos[idx++] = data_len >> 8;
        pos[idx++] = data_len & 0xFF;
        memcpy(&(pos[idx]), general_property->correlation_data.value, data_len);
        idx += data_len;
    }

    /* Subscription Identifier */
    if (general_property->subscription_identifier > 0) {
        uint32_t subscription_identifer_offset = 0;
        uint8_t subscription_identifier[4] = {0};
        _service_mqtt_remain_len_encode(general_property->subscription_identifier, &subscription_identifier[0],
                                     &subscription_identifer_offset);
        pos[idx++] = SERVICE_MQTTPROP_SUBSCRIPTION_IDENTIFIER;
        memcpy(&pos[idx], subscription_identifier, subscription_identifer_offset);
        idx += subscription_identifer_offset;
    }

    /* Reason String */
    if (NULL != reason_string) {
        pos[idx++] =  SERVICE_MQTTPROP_REASON_STRING;
        pos[idx++] = (reason_string->len) >> 8 ;
        pos[idx++] =  reason_string->len & 0xFF;
        memcpy(&pos[idx], reason_string->value, reason_string->len);
        idx += reason_string->len ;
    }

    *index = idx;
}

static int32_t _read_variable_byte_interger(uint8_t *input, uint32_t *remainlen, uint8_t *offset);
static int32_t _service_mqtt_conn_pkt(service_mq_process_t *mq_process, uint8_t **pkt, uint32_t *pkt_len,
                                   conn_property_t *conn_prop)
{
    uint32_t idx = 0, conn_paylaod_len = 0, conn_remainlen = 0, conn_pkt_len = 0, property_len = 0, property_total_len = 0;
    uint8_t *pos = NULL;
    const uint8_t conn_fixed_header = SERVICE_MQTT_CONN_PKT_TYPE;
    const uint8_t conn_protocol_name[] = {0x00, 0x04, 0x4D, 0x51, 0x54, 0x54};
    uint8_t conn_protocol_level = 0x04;
    const uint8_t conn_connect_flag = 0xC0 | (mq_process->clean_session << 1);
    uint32_t property_len_offset = 0;
    uint8_t property_len_array[4] = {0};
    general_property_t general_property = {0};

    /* Property Len */
    if (_service_mqtt_5_feature_is_enabled(mq_process)) {
        conn_protocol_level = 0x5;
        if (NULL != conn_prop) {
            general_property.topic_alias_max = conn_prop->topic_alias_max;
            memcpy(&(general_property.user_property[0]), &(conn_prop->user_property[0]),
                   MQTT5_ATTRIBUTE_MAX * (sizeof(mqtt5_property_t *)));
        }

        if (NULL == mq_process->pre_connect_property) {
            /* Normal Connection Case */
            int res = _service_mqtt_general_property_is_valid(&general_property);
            if (res < RET_SUCCESS) {
                return res;
            }

            property_len = _service_get_general_property_len(&general_property);
            _service_mqtt_remain_len_encode(property_len, property_len_array, &property_len_offset);
        } else {
            /* Reconnetion Case: Use Connection Properties Stored Previously */
            uint8_t tmp_offset = 0;
            _read_variable_byte_interger(mq_process->pre_connect_property, &property_len, &tmp_offset);
            property_len_offset = tmp_offset;
        }

        property_total_len = property_len_offset + property_len;
    }

    /* Payload Length */
    conn_paylaod_len = (uint32_t)(strlen(mq_process->clientid) + strlen(mq_process->username)
                                  + strlen(mq_process->password) + 3 * SERVICE_MQTT_UTF8_STR_EXTRA_LEN);

    /* Remain-Length Value */
    conn_remainlen = SERVICE_MQTT_CONN_REMAINLEN_FIXED_LEN + conn_paylaod_len + property_total_len;

    /* Total Packet Length */
    conn_pkt_len = SERVICE_MQTT_CONN_FIXED_HEADER_TOTAL_LEN + conn_paylaod_len + property_total_len;

    pos = mq_process->sysdep->service_sysdep_malloc(conn_pkt_len);
    if (pos == NULL) {
        return RET_SYS_DEPEND_MALLOC_FAILED;
    }
    memset(pos, 0, conn_pkt_len);

    /* Fixed Header */
    pos[idx++] = conn_fixed_header;

    /* Remain Length */
    _service_mqtt_remain_len_encode(conn_remainlen, pos + idx, &idx);

    /* Protocol Name */
    memcpy(pos + idx, conn_protocol_name, SERVICE_MQTT_CONN_PROTOCOL_NAME_LEN);
    idx += SERVICE_MQTT_CONN_PROTOCOL_NAME_LEN;

    /* Protocol Level */
    pos[idx++] = conn_protocol_level;

    /* Connect Flag */
    pos[idx++] = conn_connect_flag;

    /* Keep Alive MSB */
    pos[idx++] = (uint8_t)((mq_process->keep_alive_s >> 8) & 0x00FF);

    /* Keep Alive LSB */
    pos[idx++] = (uint8_t)((mq_process->keep_alive_s) & 0x00FF);

    /* property */
    if (_service_mqtt_5_feature_is_enabled(mq_process)) {
        if (NULL != mq_process->pre_connect_property) {
            /* Normal Connection Case: Use Connection Properties Stored Previously */
            memcpy(&(pos[idx]), mq_process->pre_connect_property, property_total_len);
            idx += property_total_len;
        } else  {
            /* Not Reconnection Case */
            uint8_t *start = &(pos[idx]);
            _service_write_general_prop(pos, &idx, &general_property, property_len_array, property_len_offset);
            mq_process->pre_connect_property = mq_process->sysdep->service_sysdep_malloc(property_total_len);
            if (NULL == mq_process->pre_connect_property) {
                return RET_SYS_DEPEND_MALLOC_FAILED;
            }
            memset(mq_process->pre_connect_property, 0, property_total_len);
            memcpy(mq_process->pre_connect_property, start, property_total_len);
        }
    }

    /* Payload: clientid, username, password */
    _service_mqtt_set_utf8_encoded_str((uint8_t *)mq_process->clientid, strlen(mq_process->clientid), pos + idx);
    idx += SERVICE_MQTT_UTF8_STR_EXTRA_LEN + strlen(mq_process->clientid);

    _service_mqtt_set_utf8_encoded_str((uint8_t *)mq_process->username, strlen(mq_process->username), pos + idx);
    idx += SERVICE_MQTT_UTF8_STR_EXTRA_LEN + strlen(mq_process->username);

    _service_mqtt_set_utf8_encoded_str((uint8_t *)mq_process->password, strlen(mq_process->password), pos + idx);
    idx += SERVICE_MQTT_UTF8_STR_EXTRA_LEN + strlen(mq_process->password);

    *pkt = pos;
    *pkt_len = idx;

    return RET_SUCCESS;
}

static int32_t _read_variable_byte_interger(uint8_t *input, uint32_t *remainlen, uint8_t *offset)
{
    uint8_t ch = 0;
    uint32_t multiplier = 1;
    uint32_t mqtt_remainlen = 0;
    uint8_t pos = 0;

    do {
        ch = input[pos++];
        mqtt_remainlen += (ch & 127) * multiplier;
        if (multiplier > 128 * 128 * 128) {
            return RET_MQTT_MALFORMED_REMAINING_LEN;
        }
        multiplier *= 128;
    } while ((ch & 128) != 0 && pos < 4);

    *remainlen = mqtt_remainlen;
    *offset = pos;

    return RET_SUCCESS;
}

static int32_t _service_mqtt_parse_property_element(service_mq_process_t *mq_process, uint8_t *property, uint32_t *idx,
        type_mqtt5_property_element_t *tlv,
        mqtt5_property_t *user_prop, uint32_t *variable_byte_integer)
{
    uint32_t read_len = 0;
    uint8_t type = property[0];
    int32_t res;

    switch (type) {
        /* Bits (Single Byte) */
        case SERVICE_MQTTPROP_PAYLOAD_FORMAT_INDICATOR:
        case SERVICE_MQTTPROP_REQUEST_PROBLEM_INFORMATION:
        case SERVICE_MQTTPROP_REQUEST_RESPONSE_INFORMATION:
        case SERVICE_MQTTPROP_MAX_QOS:
        case SERVICE_MQTTPROP_RETAIN_AVAILABLE:
        case SERVICE_MQTTPROP_WILDCARD_SUBSCRIPTION_AVAILABLE:
        case SERVICE_MQTTPROP_SUBSCRIPTION_IDENTIFIER_AVAILABLE:
        case SERVICE_MQTTPROP_SHARED_SUBSCRIPTION_AVAILABLE: {
            read_len++;
            tlv->type = type;
            tlv->len = 1;
            read_len += 1;
            tlv->value = property + SERVICE_MQTT_V5_PROPERTY_ID_LEN ;
        }
        break;

        /* Two Byte Integer */
        case SERVICE_MQTTPROP_SERVER_KEEP_ALIVE:
        case SERVICE_MQTTPROP_TOPIC_ALIAS:
        case SERVICE_MQTTPROP_RECEIVE_MAXIMUM:
        case SERVICE_MQTTPROP_TOPIC_ALIAS_MAX: {
            read_len++;
            tlv->type = type;
            tlv->len = 2;
            read_len += 2;
            tlv->value = property + SERVICE_MQTT_V5_PROPERTY_ID_LEN;
        }
        break;

        /* Four Byte Integer */
        case SERVICE_MQTTPROP_PUBLICATION_EXPIRY_INTERVAL:
        case SERVICE_MQTTPROP_SESSION_EXPIRY_INTERVAL:
        case SERVICE_MQTTPROP_WILL_DELAY_INTERVAL:
        case SERVICE_MQTTPROP_MAX_PACK_SIZE: {
            read_len++;
            tlv->type = type;
            tlv->len = 4;
            read_len += 4;
            tlv->value = property + SERVICE_MQTT_V5_PROPERTY_ID_LEN;
        }
        break;

        /* Variable Byte Integer */
        case SERVICE_MQTTPROP_SUBSCRIPTION_IDENTIFIER: {
            read_len++;
            uint8_t *remain_len_start = property + read_len;
            uint32_t remain_len = 0;
            uint8_t offset = 0;

            res = _read_variable_byte_interger(remain_len_start, &remain_len, &offset);
            if (res < RET_SUCCESS) {
                return res;
            }
            read_len += offset;

            tlv->type = type;
            tlv->len = offset;
            *variable_byte_integer = remain_len;
        }
        break;

        /* Binary Data; UTF-8 String */
        case SERVICE_MQTTPROP_CONTENT_TYPE:
        case SERVICE_MQTTPROP_RESPONSE_TOPIC:
        case SERVICE_MQTTPROP_ASSIGNED_CLIENT_IDENTIFIER:
        case SERVICE_MQTTPROP_AUTHENTICATION_METHOD:
        case SERVICE_MQTTPROP_RESPONSE_INFORMATION:
        case SERVICE_MQTTPROP_SERVER_REFERENCE:
        case SERVICE_MQTTPROP_REASON_STRING:
        case SERVICE_MQTTPROP_CORRELATION_DATA:
        case SERVICE_MQTTPROP_AUTHENTICATION_DATA: {
            read_len++;
            tlv->type = type;
            tlv->len = ((*(property + 1)) << 8) + (*(property + 2));
            read_len += 2;
            tlv->value = property + 3;
            read_len += tlv->len;
        }
        break;

        /* UTF-8 String Pairs. especially for User Properties */
        case SERVICE_MQTTPROP_USER_PROPERTY: {
            read_len++;
            uint8_t *key_start = property + read_len;
            user_prop->key.len = ((*(key_start)) << 8) + (*(key_start + 1));
            user_prop->key.value = key_start + 2;
            read_len += 2 + user_prop->key.len;

            uint8_t *value_start = property + read_len;
            user_prop->value.len = ((*(value_start)) << 8) + (*(value_start + 1));
            user_prop->value.value = value_start + 2;
            read_len += 2 + user_prop->value.len;

            tlv->type = type;
            tlv->len = 2 + user_prop->key.len + 2 + user_prop->value.len;
        }
        break;

        default: {
            tlv->type = SERVICE_MQTTPROP_UNRESOLVED;
            log_debug("unresolved property option\n");
        }
        break;
    }
    *idx = *idx + read_len;
    return RET_SUCCESS;
}

static int32_t _service_mqtt_parse_general_properties(service_mq_process_t *mq_process, uint8_t *property,
        general_property_t *general_prop, uint32_t *parsed_property_len, uint8_t *parsed_offset, uint32_t remain_len)
{
    uint32_t idx = 0, total_prop_len = 0;
    int32_t res;
    mqtt5_property_t **user_prop = general_prop->user_property;
    uint32_t property_len = 0;
    uint8_t property_len_offset = 0;
    type_mqtt5_property_element_t tlv = {0};
    mqtt5_property_t cur_user_prop;
    uint8_t user_property_count = 0;
    uint32_t variable_byte_integer = 0;

    res = _read_variable_byte_interger(&(property[0]), &property_len,  &property_len_offset);
    if (res < RET_SUCCESS) {
        return res;
    }

    /* quit if the total len of all properties exceeds the remain_len of mqtt packet */
    if (property_len > remain_len - property_len_offset) {
        return RET_MQTT_INVALID_PROPERTY_LEN;
    }

    if (NULL != parsed_property_len) {
        *parsed_property_len = property_len;
    }
    if (NULL != parsed_offset) {
        *parsed_offset = property_len_offset;
    }

    total_prop_len = property_len + property_len_offset;
    if (property_len == 0) {
        return RET_SUCCESS;
    }

    idx += property_len_offset;
    general_prop->max_qos = 2;                            /* by default is 2 */
    general_prop->wildcard_subscription_available = 1;    /* enabled by default */
    general_prop->subscription_identifier_available = 1;  /* enabled by default */
    general_prop->shared_subscription_available = 1;      /* enabled by default */

    while (total_prop_len > idx) {

        memset(&tlv, 0, sizeof(tlv));
        memset(&cur_user_prop, 0, sizeof(cur_user_prop));
        res = _service_mqtt_parse_property_element(mq_process, property + idx, &idx, &tlv, &cur_user_prop,
                                                &variable_byte_integer);
        if (res < RET_SUCCESS) {
            return res;
        }

        /* quit if the length of a certain property exceeds the remain_len of the whole packet */
        if (idx > remain_len) {
            return RET_MQTT_INVALID_PROPERTY_LEN;
        }

        switch (tlv.type) {
            case SERVICE_MQTTPROP_TOPIC_ALIAS: {
                uint16_t topic_alias = 0;
                topic_alias = (uint16_t)(*(tlv.value) << 8) ;
                topic_alias += (uint16_t)(*(tlv.value + 1));
                general_prop->topic_alias = topic_alias;
            }
            break;
            case SERVICE_MQTTPROP_TOPIC_ALIAS_MAX: {
                uint16_t topic_alias_max = 0;
                topic_alias_max = (uint16_t)(*(tlv.value) << 8) ;
                topic_alias_max += (uint16_t)(*(tlv.value + 1));
                general_prop->topic_alias_max = topic_alias_max;
            }
            break;
            case SERVICE_MQTTPROP_MAX_QOS: {
                uint8_t max_qos = 0;
                max_qos = (uint8_t)(*(tlv.value));
                general_prop->max_qos = max_qos;
            }
            break;
            case SERVICE_MQTTPROP_ASSIGNED_CLIENT_IDENTIFIER: {
                void *cid = mq_process->sysdep->service_sysdep_malloc(tlv.len + 1);
                if (NULL == cid) {
                    return RET_SYS_DEPEND_MALLOC_FAILED;
                }
                memset(cid, 0, tlv.len + 1);
                memcpy(cid, tlv.value, tlv.len);
                general_prop->assigned_clientid = cid;
            }
            break;
            case SERVICE_MQTTPROP_WILDCARD_SUBSCRIPTION_AVAILABLE: {
                general_prop->wildcard_subscription_available = (uint8_t)(*(tlv.value));
            }
            break;
            case SERVICE_MQTTPROP_SUBSCRIPTION_IDENTIFIER_AVAILABLE: {
                general_prop->subscription_identifier_available = (uint8_t)(*(tlv.value));
            }
            break;
            case SERVICE_MQTTPROP_SHARED_SUBSCRIPTION_AVAILABLE: {
                general_prop->shared_subscription_available = (uint8_t)(*(tlv.value));
            }
            break;
            case SERVICE_MQTTPROP_PUBLICATION_EXPIRY_INTERVAL: {
                uint32_t message_expire = 0;
                message_expire += *(tlv.value) << 24;
                message_expire += *(tlv.value + 1) << 16;
                message_expire += *(tlv.value + 2) << 8;
                message_expire += *(tlv.value + 3) << 0;
                general_prop->message_expire_interval = message_expire;
            }
            break;
            case SERVICE_MQTTPROP_MAX_PACK_SIZE: {
                uint32_t max_pack_size = 0;
                max_pack_size += *(tlv.value) << 24;
                max_pack_size += *(tlv.value + 1) << 16;
                max_pack_size += *(tlv.value + 2) << 8;
                max_pack_size += *(tlv.value + 3) << 0;
                general_prop->max_packet_size = max_pack_size;
            }
            break;
            case SERVICE_MQTTPROP_RECEIVE_MAXIMUM: {
                uint16_t server_receive_max = 0;
                server_receive_max = (uint16_t)(*(tlv.value) << 8) ;
                server_receive_max += (uint16_t)(*(tlv.value + 1));
                general_prop->server_receive_max = server_receive_max;
            }
            break;
            case SERVICE_MQTTPROP_USER_PROPERTY: {
                if (user_property_count < MQTT5_ATTRIBUTE_MAX && cur_user_prop.key.len != 0) {
                    user_prop[user_property_count] = mq_process->sysdep->service_sysdep_malloc(sizeof(mqtt5_property_t));
                    if (NULL == user_prop[user_property_count]) {
                        return RET_SYS_DEPEND_MALLOC_FAILED;
                    }
                    memcpy(user_prop[user_property_count], &cur_user_prop, sizeof(mqtt5_property_t));
                    user_property_count++;
                }
            }
            break;
            case SERVICE_MQTTPROP_UNRESOLVED: {
                return RET_MQTT_UNKNOWN_PROPERTY_OPTION;
            }
            default:
                break;
        }
    }

    return RET_SUCCESS;
}


static int32_t _service_mqtt_parse_conack_properties(service_mq_process_t *mq_process, uint8_t *property,
        connack_property_t *conack_prop, uint32_t remain_len)
{
    general_property_t general_prop = {0};
    int32_t res = _service_mqtt_parse_general_properties(mq_process, property, &general_prop, NULL, NULL, remain_len);
    if (res != RET_SUCCESS) {
        return res;
    }

    conack_prop->max_qos = general_prop.max_qos;
    conack_prop->topic_alias_max = general_prop.topic_alias_max;
    memcpy(&(conack_prop->user_property[0]), &(general_prop.user_property[0]),
           MQTT5_ATTRIBUTE_MAX * (sizeof(mqtt5_property_t *)));
    conack_prop->assigned_clientid = general_prop.assigned_clientid;
    conack_prop->max_packet_size = general_prop.max_packet_size;
    conack_prop->server_receive_max = general_prop.server_receive_max;
    conack_prop->wildcard_subscription_available = general_prop.wildcard_subscription_available;
    conack_prop->subscription_identifier_available = general_prop.subscription_identifier_available;
    conack_prop->shared_subscription_available = general_prop.shared_subscription_available;
    mq_process->tx_topic_alias_max =
                conack_prop->topic_alias_max;

    return RET_SUCCESS;
}

static void _service_user_properties(service_mq_process_t *mq_process, mqtt5_property_t **user_property_list)
{
    int i = 0;
    mqtt5_property_t *prop = NULL;
    for (i = 0; i < MQTT5_ATTRIBUTE_MAX; i++) {
        prop = user_property_list[i];
        if (NULL == prop) {
            continue;
        }
        mq_process->sysdep->service_sysdep_free(prop);
        user_property_list[i] = NULL;
    }
}

static int32_t _service_mqtt_connack_ctx(service_mq_process_t *mq_process, uint8_t *connack, uint32_t remain_len)
{
    int32_t res = RET_SUCCESS;
    int32_t  ret = RET_SUCCESS;
    return ret;
    if (_service_mqtt_5_feature_is_enabled(mq_process)) {
        connack_property_t connack_prop = {0};
        tiot_mqtt_recv_t packet = {0};

        if (connack[0] != 0x00) {
            return RET_MQTT_CONNACK_FMT_ERROR;
        }

        /* First Byte is Conack Flag */
        if (connack[1] == SERVICE_MQTT_CONNACK_RCODE_ACCEPTED) {
            res = RET_SUCCESS;
        } else if (connack[1] == SERVICE_MQTT_V5_CONNACK_RCODE_UNACCEPTABLE_PROTOCOL_VERSION) {
            log_debug("MQTT invalid protocol version, disconeect\r\n");
            res = RET_MQTT_CONNACK_RCODE_UNACCEPTABLE_PROTOCOL_VERSION;
        } else if (connack[1] == SERVICE_MQTT_V5_CONNACK_RCODE_SERVER_UNAVAILABLE) {
            log_debug("MQTT server unavailable, disconnect\r\n");
            res = RET_MQTT_CONNACK_RCODE_SERVER_UNAVAILABLE;
        } else if (connack[1] == SERVICE_MQTT_V5_CONNACK_RCODE_BAD_USERNAME_PASSWORD) {
            log_debug("MQTT bad username or password, disconnect\r\n");
            res = RET_MQTT_CONNACK_RCODE_BAD_USERNAME_PASSWORD;
        } else if (connack[1] == SERVICE_MQTT_V5_CONNACK_RCODE_NOT_AUTHORIZED) {
            log_debug("MQTT authorize fail, disconnect1\r\n");
            res = RET_MQTT_CONNACK_RCODE_NOT_AUTHORIZED;
        } else {
            res = RET_MQTT_CONNACK_RCODE_UNKNOWN;
        }

        /* Second Byte is Reason Code */
        packet.data.con_ack.reason_code = connack[1];

        /* Properties Starts from 3rd Byte */
        ret = _service_mqtt_parse_conack_properties(mq_process, &connack[2], &connack_prop, remain_len);
        if (ret < RET_SUCCESS) {
            return ret;
        }

        packet.data.con_ack.prop = connack_prop;
        packet.type = TIOT_MQTTRECV_CON_ACK;

        if (mq_process->recv_ctx) {
            mq_process->recv_ctx((void *)mq_process, &packet, mq_process->userdata);
        }

        if (NULL != connack_prop.assigned_clientid) {
            mq_process->sysdep->service_sysdep_free(connack_prop.assigned_clientid);
        }

        _service_user_properties(mq_process, connack_prop.user_property);

    } else {

        if (connack[1] == SERVICE_MQTT_CONNACK_RCODE_ACCEPTED) {
            res = RET_SUCCESS;
        } else if (connack[1] == SERVICE_MQTT_CONNACK_RCODE_UNACCEPTABLE_PROTOCOL_VERSION) {
            log_debug("MQTT invalid protocol version, disconeect\r\n");
            res = RET_MQTT_CONNACK_RCODE_UNACCEPTABLE_PROTOCOL_VERSION;
        } else if (connack[1] == SERVICE_MQTT_CONNACK_RCODE_SERVER_UNAVAILABLE) {
            log_debug("MQTT server unavailable, disconnect\r\n");
            res = RET_MQTT_CONNACK_RCODE_SERVER_UNAVAILABLE;
        } else if (connack[1] == SERVICE_MQTT_CONNACK_RCODE_BAD_USERNAME_PASSWORD) {
            log_debug("MQTT bad username or password, disconnect\r\n");
            res = RET_MQTT_CONNACK_RCODE_BAD_USERNAME_PASSWORD;
        } else if (connack[1] == SERVICE_MQTT_CONNACK_RCODE_NOT_AUTHORIZED) {
            log_debug("MQTT authorize fail, disconnect2\r\n");
            res = RET_MQTT_CONNACK_RCODE_NOT_AUTHORIZED;
        } else {
            res = RET_MQTT_CONNACK_RCODE_UNKNOWN;
        }
    }
    return res;
}

static int32_t _service_mqtt_read(service_mq_process_t *mq_process, uint8_t *buffer, uint32_t len, uint32_t timeout_ms)
{
    int32_t res = RET_SUCCESS;

    if (mq_process->network_ctx != NULL) {
        res = mq_process->sysdep->service_sysdep_network_recv(mq_process->network_ctx, buffer, len, timeout_ms, NULL);
        if (res < RET_SUCCESS) {
            res = _service_mqtt_sysdep_return(res, RET_SYS_DEPEND_NWK_RECV_ERR);
        } else if (res != len) {
            res = RET_SYS_DEPEND_NWK_READ_LESSDATA;
        }
    } else {
        res = RET_SYS_DEPEND_NWK_CLOSED;
    }

    return res;
}

static int32_t _service_mqtt_write(service_mq_process_t *mq_process, uint8_t *buffer, uint32_t len, uint32_t timeout_ms)
{
    int32_t res = RET_SUCCESS;

    if (mq_process->network_ctx != NULL) {
        res = mq_process->sysdep->service_sysdep_network_send(mq_process->network_ctx, buffer, len, timeout_ms, NULL);
        if (res < RET_SUCCESS) {
            res = _service_mqtt_sysdep_return(res, RET_SYS_DEPEND_NWK_SEND_ERR);
        } else if (res != len) {
            res = RET_SYS_DEPEND_NWK_WRITE_LESSDATA;
        }
    } else {
        res = RET_SYS_DEPEND_NWK_CLOSED;
    }

    return res;
}


static void _service_mqtt_connect_diag(service_mq_process_t *mq_process, uint8_t flag)
{
    uint8_t buf[4] = {0};

    buf[0] = (0x0010 >> 8) & 0x00FF;
    buf[1] = (0x0010) & 0x00FF;
    buf[2] = 0x01;
    buf[3] = flag;

    service_diag(mq_process->sysdep, RET_MQTT_BASE, buf, sizeof(buf));
}

static void _service_mqtt_heartbeat_diag1(service_mq_process_t *mq_process)
{
    if(mq_process == NULL){
        return;
    }
    uint8_t buf[4] = {0};

    buf[0] = (0x0020 >> 8) & 0x00FF;
    buf[1] = (0x0020) & 0x00FF;
    buf[2] = 0x01;
    buf[3] = 0x01;

    service_diag(mq_process->sysdep, RET_MQTT_BASE, buf, sizeof(buf));
}

static void _service_mqtt_heartbeat_diag0(service_mq_process_t *mq_process)
{
    if(mq_process == NULL){
        return;
    }
    uint8_t buf[4] = {0};

    buf[0] = (0x0020 >> 8) & 0x00FF;
    buf[1] = (0x0020) & 0x00FF;
    buf[2] = 0x01;
    buf[3] = 0x00;

    service_diag(mq_process->sysdep, RET_MQTT_BASE, buf, sizeof(buf));
}

static int32_t _service_mqtt_read_remainlen(service_mq_process_t *mq_process, uint32_t *remainlen);


static int32_t _service_mqtt_connect(service_mq_process_t *mq_process, conn_property_t *conn_prop)
{
    int32_t res = 0;
    service_sysdep_socket_type_t socket_type = SERVICE_SYSDEP_SOCKET_TCP_CLIENT;
    char backup_ip[16] = {0};
    uint8_t *conn_pkt = NULL;
    uint8_t connack_fixed_header = 0;
    uint8_t *connack_ptr = NULL;
    uint32_t conn_pkt_len = 0;
    char *secure_mode = (mq_process->cred == NULL) ? ("3") : ("2");
    uint8_t use_assigned_clientid = mq_process->use_assigned_clientid;
    uint32_t remain_len = 0;

    if (mq_process->host == NULL) {
        return RET_USER_INPUT_MISSING_HOST;
    }
    log_debug("service_sysdep_network_establish host %s \r\n", mq_process->host);

    if (mq_process->security_mode != NULL) {
        secure_mode = mq_process->security_mode;
    }

    if (mq_process->cred && \
        mq_process->cred->option == TIOT_SYSDEP_NETWORK_CRED_NONE && \
        mq_process->security_mode == NULL) {
        secure_mode = "3";
    }

    if (mq_process->username == NULL || mq_process->password == NULL ||
        mq_process->clientid == NULL) {
        /* no valid username, password or clientid, check pk/dn/ds */
        if (mq_process->product_key == NULL) {
            return RET_USER_INPUT_MISSING_PRODUCT_KEY;
        }
        if (mq_process->device_name == NULL) {
            return RET_USER_INPUT_MISSING_DEVICE_NAME;
        }
        if (mq_process->device_secret == NULL) {
            return RET_USER_INPUT_MISSING_DEVICE_SECRET;
        }
        _service_mqtt_sign_clean(mq_process);
        mq_process->auth_timestamp = 0;

        if ((res = service_auth_mqtt_username(mq_process->sysdep, &mq_process->username, mq_process->product_key,
                                           mq_process->device_name)) < RET_SUCCESS ||
            (res = service_auth_mqtt_password(mq_process->sysdep, &mq_process->password, mq_process->auth_timestamp, mq_process->product_key,
                                           mq_process->device_name, mq_process->device_secret, use_assigned_clientid)) < RET_SUCCESS ||
            (res = service_auth_mqtt_clientid(mq_process->sysdep, &mq_process->clientid, mq_process->auth_timestamp, mq_process->product_key,
                                           mq_process->device_name, secure_mode, mq_process->extend_clientid, use_assigned_clientid)) < RET_SUCCESS) {
            _service_mqtt_sign_clean(mq_process);
            return res;
        }
        log_debug("nm:%s\r\n", (void *)mq_process->username);
        log_debug("pw:%s\r\n", (void *)mq_process->password);
        log_debug("id:%s\r\n", (void *)mq_process->clientid); 
    }

    if (mq_process->network_ctx != NULL) {
        mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
    }

    _service_mqtt_connect_diag(mq_process, 0x00);

    mq_process->network_ctx = mq_process->sysdep->service_sysdep_network_init();
    if (mq_process->network_ctx == NULL) {
        return RET_SYS_DEPEND_MALLOC_FAILED;
    }

    service_global_get_backup_ip(mq_process->sysdep, backup_ip);
    if (strlen(backup_ip) > 0) {
        log_debug("backup_ip:%s\r\n", (void *)backup_ip);
    }
    if ((res = mq_process->sysdep->service_sysdep_network_setopt(mq_process->network_ctx, SERVICE_SYSDEP_NETWORK_SOCKET_TYPE,
               &socket_type)) < RET_SUCCESS ||
        (res = mq_process->sysdep->service_sysdep_network_setopt(mq_process->network_ctx, SERVICE_SYSDEP_NETWORK_HOST,
                mq_process->host)) < RET_SUCCESS ||
        (res = mq_process->sysdep->service_sysdep_network_setopt(mq_process->network_ctx, SERVICE_SYSDEP_NETWORK_BACKUP_IP,
                backup_ip)) < RET_SUCCESS ||
        (res = mq_process->sysdep->service_sysdep_network_setopt(mq_process->network_ctx, SERVICE_SYSDEP_NETWORK_PORT,
                &mq_process->port)) < RET_SUCCESS ||
        (res = mq_process->sysdep->service_sysdep_network_setopt(mq_process->network_ctx,
                SERVICE_SYSDEP_NETWORK_CONNECT_TIMEOUT_MS,
                &mq_process->connect_timeout_ms)) < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
        return _service_mqtt_sysdep_return(res, RET_SYS_DEPEND_NWK_INVALID_OPTION);
    }

    if (mq_process->cred != NULL) {
        if ((res = mq_process->sysdep->service_sysdep_network_setopt(mq_process->network_ctx, SERVICE_SYSDEP_NETWORK_CRED,
                   mq_process->cred)) < RET_SUCCESS) {
            mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
            return _service_mqtt_sysdep_return(res, RET_SYS_DEPEND_NWK_INVALID_OPTION);
        }
        if (mq_process->cred->option == TIOT_SYSDEP_NETWORK_CRED_SVRCERT_PSK) {
            char *psk_id = NULL, psk[65] = {0};
            service_sysdep_psk_t sysdep_psk;

            res = service_auth_tls_psk(mq_process->sysdep, &psk_id, psk, mq_process->product_key, mq_process->device_name,
                                    mq_process->device_secret);
            if (res < RET_SUCCESS) {
                return res;
            }

            memset(&sysdep_psk, 0, sizeof(service_sysdep_psk_t));
            sysdep_psk.psk_id = psk_id;
            sysdep_psk.psk = psk;
            log_debug("psk_id:%s\r\n", sysdep_psk.psk_id);
            log_debug("psk:%s\r\n", sysdep_psk.psk);
            res = mq_process->sysdep->service_sysdep_network_setopt(mq_process->network_ctx, SERVICE_SYSDEP_NETWORK_PSK,
                    (void *)&sysdep_psk);
            mq_process->sysdep->service_sysdep_free(psk_id);
            if (res < RET_SUCCESS) {
                return _service_mqtt_sysdep_return(res, RET_SYS_DEPEND_NWK_INVALID_OPTION);
            }
        }
        mq_process->nwkstats_info.network_type = (uint8_t)mq_process->cred->option;
    }

    /* Remove All Topic talkweb Relationship */
    _service_mqtt_topic_alias_list_remove_all(mq_process);

    /* network stats */
    mq_process->nwkstats_info.connect_timestamp = mq_process->sysdep->service_sysdep_time();

    if ((res = mq_process->sysdep->service_sysdep_network_establish(mq_process->network_ctx)) < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
        mq_process->nwkstats_info.failed_timestamp = mq_process->nwkstats_info.connect_timestamp;
        mq_process->nwkstats_info.failed_error_code = res;
        return _service_mqtt_sysdep_return(res, RET_SYS_DEPEND_NWK_EST_FAILED);
    }

    mq_process->nwkstats_info.connect_time_used = mq_process->sysdep->service_sysdep_time() \
            - mq_process->nwkstats_info.connect_timestamp;


    /* Get MQTT Connect Packet */
    res = _service_mqtt_conn_pkt(mq_process, &conn_pkt, &conn_pkt_len, conn_prop);
    if (res < RET_SUCCESS) {
        return res;
    }
    log_debug("conn_pkt_len:%d\n", &conn_pkt_len);
    /* Send MQTT Connect Packet */
    res = _service_mqtt_write(mq_process, conn_pkt, conn_pkt_len, mq_process->send_timeout_ms);
    mq_process->sysdep->service_sysdep_free(conn_pkt);
    if (res < RET_SUCCESS) {
        if (res == RET_SYS_DEPEND_NWK_WRITE_LESSDATA) {
            log_error("MQTT connect packet send timeout: %d\r\n",
                      mq_process->send_timeout_ms);
        } else {
            if (mq_process->network_ctx != NULL) {
                mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
            }
        }
        return res;
    }

    /* Receive MQTT Connect ACK Fixed Header Byte */
    res = _service_mqtt_read(mq_process, &connack_fixed_header, SERVICE_MQTT_FIXED_HEADER_LEN, mq_process->recv_timeout_ms);
    if (res < RET_SUCCESS) {
        if (res == RET_SYS_DEPEND_NWK_READ_LESSDATA) {
            log_error("MQTT connack packet recv fixed header timeout: %d\r\n",
                      mq_process->recv_timeout_ms);
        } else {
            if (mq_process->network_ctx != NULL) {
                mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
            }
        }
        return res;
    }
    log_debug("connack_fixed_header:%x\n", connack_fixed_header);

    if (connack_fixed_header != SERVICE_MQTT_CONNACK_PKT_TYPE) {
        return RET_MQTT_CONNACK_FMT_ERROR;
    }

    /* Receive MQTT Connect ACK remain len */
    res = _service_mqtt_read_remainlen(mq_process, &remain_len);
    if (res < RET_SUCCESS) {
        if (res == RET_SYS_DEPEND_NWK_READ_LESSDATA) {
            log_debug("MQTT connack packet recv remain len timeout: %d\r\n",
                      mq_process->recv_timeout_ms);
        }
        return res;
    }
    log_debug("remain_len:%d\n", remain_len);

    /* Connack Format Error for Mqtt 3.1 */
    if (0 == _service_mqtt_5_feature_is_enabled(mq_process) && remain_len != 0x2) {
        return RET_MQTT_CONNACK_FMT_ERROR;
    }

    connack_ptr = mq_process->sysdep->service_sysdep_malloc(remain_len);
    if (NULL == connack_ptr) {
        return RET_SYS_DEPEND_MALLOC_FAILED;
    }
    memset(connack_ptr, 0, remain_len);

    /* Receive MQTT Connect ACK Variable Header and Properties */
    res = _service_mqtt_read(mq_process, connack_ptr, remain_len, mq_process->recv_timeout_ms);
    if (res < RET_SUCCESS) {
        if (res == RET_SYS_DEPEND_NWK_READ_LESSDATA) {
            log_debug("MQTT connack packet variable header and property recv timeout: %d\r\n",
                      mq_process->recv_timeout_ms);
        }
        mq_process->sysdep->service_sysdep_free(connack_ptr);
        return res;
    }

    res = _service_mqtt_connack_ctx(mq_process, connack_ptr, remain_len);
    mq_process->sysdep->service_sysdep_free(connack_ptr);
    if (res < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
        return res;
    }

    _service_mqtt_connect_diag(mq_process, 0x01);

    return RET_MQTT_CONNECT_SUCCESS;

}

static int32_t _service_mqtt_disconnect(service_mq_process_t *mq_process)
{
    if(mq_process == NULL){
        log_error("param error");
        return RET_USER_INPUT_NULL_POINTER;
    }

    int32_t res = 0;
    uint8_t pingreq_pkt[2] = {SERVICE_MQTT_DISCONNECT_PKT_TYPE, 0x00};

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    res = _service_mqtt_write(mq_process, pingreq_pkt, 2, mq_process->send_timeout_ms);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
    if (res < RET_SUCCESS) {
        if (res != RET_SYS_DEPEND_NWK_WRITE_LESSDATA) {
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
            if (mq_process->network_ctx != NULL) {
                mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
            }
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
        }
        return res;
    }

    return RET_SUCCESS;
}

static uint16_t _service_mqtt_packet_id(service_mq_process_t *mq_process)
{
    if(mq_process == NULL){
        log_error("param error");
        return RET_USER_INPUT_NULL_POINTER;
    }
    uint16_t packet_id = 0;

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    if ((uint16_t)(mq_process->packet_id + 1) != 0) {
        packet_id = ++mq_process->packet_id;
    } else{
        mq_process->packet_id = 0;
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);

    return packet_id;
}

static int32_t _service_mqtt_publist_insert(service_mq_process_t *mq_process, uint8_t *packet, uint32_t len,
        uint16_t packet_id)
{
    service_mqtt_pub_node_t *node = NULL;

    service_list_for_each_entry(node, &mq_process->pub_list, linked_node, service_mqtt_pub_node_t) {
        if (node->packet_id == packet_id) {
            return RET_MQTT_PUBLIST_PACKET_ID_ROLL;
        }
    }

    node = mq_process->sysdep->service_sysdep_malloc(sizeof(service_mqtt_pub_node_t));
    if (node == NULL) {
        return RET_SYS_DEPEND_MALLOC_FAILED;
    }
    memset(node, 0, sizeof(service_mqtt_pub_node_t));
    CORE_INIT_LIST_HEAD(&node->linked_node);
    node->packet_id = packet_id;
    node->packet = mq_process->sysdep->service_sysdep_malloc(len);
    if (node->packet == NULL) {
        mq_process->sysdep->service_sysdep_free(node);
        return RET_SYS_DEPEND_MALLOC_FAILED;
    }
    memset(node->packet, 0, len);
    memcpy(node->packet, packet, len);
    node->len = len;
    node->last_send_time = mq_process->sysdep->service_sysdep_time();

    service_list_add_tail(&node->linked_node, &mq_process->pub_list);

    return RET_SUCCESS;
}

static void _service_mqtt_publist_remove(service_mq_process_t *mq_process, uint16_t packet_id)
{
    if(mq_process == NULL){
        log_error("param error");
        return;
    }

    service_mqtt_pub_node_t *node = NULL;
    service_mqtt_pub_node_t *next = NULL;

    service_list_for_each_entry_safe(node, next, &mq_process->pub_list,
                                  linked_node, service_mqtt_pub_node_t) {
        if (packet_id == node->packet_id) {
            service_list_del(&node->linked_node);
            mq_process->sysdep->service_sysdep_free(node->packet);
            mq_process->sysdep->service_sysdep_free(node);
            return;
        }
    }
}

static void _service_mqtt_publist_destroy(service_mq_process_t *mq_process)
{
    service_mqtt_pub_node_t *node = NULL, *next = NULL;

    service_list_for_each_entry_safe(node, next, &mq_process->pub_list,
                                  linked_node, service_mqtt_pub_node_t) {
        service_list_del(&node->linked_node);
        mq_process->sysdep->service_sysdep_free(node->packet);
        mq_process->sysdep->service_sysdep_free(node);
    }
}

static int32_t _service_mqtt_subunsub(service_mq_process_t *mq_process, char *topic, uint16_t topic_len, uint8_t qos,
                                   uint8_t pkt_type, general_property_t *general_property)
{
    int32_t res = 0;
    uint16_t packet_id = 0;
    uint8_t *pkt = NULL;
    uint32_t idx = 0, pkt_len = 0, remainlen = 0;
    uint32_t property_total_len = 0;
    uint32_t property_len = 0, property_len_offset = 0;
    uint8_t property_len_array[4] = {0};

    if (_service_mqtt_5_feature_is_enabled(mq_process)) {
        if (NULL != general_property) {
            res = _service_mqtt_general_property_is_valid(general_property);
            if (res < RET_SUCCESS) {
                return res;
            }
            property_len = _service_get_general_property_len(general_property);
        }

        /* Property Len */
        _service_mqtt_remain_len_encode(property_len, &property_len_array[0],
                                     &property_len_offset);
        property_total_len = property_len_offset + property_len;
    }

    remainlen = SERVICE_MQTT_PACKETID_LEN + SERVICE_MQTT_UTF8_STR_EXTRA_LEN + topic_len + property_total_len;
    if (pkt_type == SERVICE_MQTT_SUB_PKT_TYPE) {
        remainlen += SERVICE_MQTT_REQUEST_QOS_LEN;
    }
    pkt_len = SERVICE_MQTT_FIXED_HEADER_LEN + SERVICE_MQTT_REMAINLEN_MAXLEN + remainlen;

    pkt = mq_process->sysdep->service_sysdep_malloc(pkt_len);
    if (pkt == NULL) {
        return RET_SYS_DEPEND_MALLOC_FAILED;
    }
    memset(pkt, 0, pkt_len);

    /* Subscribe/Unsubscribe Packet Type */
    if (pkt_type == SERVICE_MQTT_SUB_PKT_TYPE) {
        pkt[idx++] = SERVICE_MQTT_SUB_PKT_TYPE | SERVICE_MQTT_SUB_PKT_RESERVE;
    } else if (pkt_type == SERVICE_MQTT_UNSUB_PKT_TYPE) {
        uint8_t unsub_flag = 0;
        if (_service_mqtt_5_feature_is_enabled(mq_process)) {
            unsub_flag = SERVICE_MQTT_UNSUB_PKT_TYPE | SERVICE_MQTT_UNSUB_PKT_RESERVE;
        } else {
            unsub_flag = SERVICE_MQTT_UNSUB_PKT_TYPE;
        }
        pkt[idx++] = unsub_flag;
    }

    /* Remaining Length */
    _service_mqtt_remain_len_encode(remainlen, &pkt[idx], &idx);

    /* Packet Id */
    packet_id = _service_mqtt_packet_id(mq_process);
    pkt[idx++] = (uint8_t)((packet_id >> 8) & 0x00FF);
    pkt[idx++] = (uint8_t)((packet_id) & 0x00FF);

    if (_service_mqtt_5_feature_is_enabled(mq_process)) {
        _service_write_general_prop(pkt, &idx, general_property, property_len_array, property_len_offset);
    }

    /* Topic */
    pkt[idx++] = (uint8_t)((topic_len >> 8) & 0x00FF);
    pkt[idx++] = (uint8_t)((topic_len) & 0x00FF);
    memcpy(&pkt[idx], topic, topic_len);
    idx += topic_len;

    /* QOS */
    if (pkt_type == SERVICE_MQTT_SUB_PKT_TYPE) {
        pkt[idx++] = qos;
    }

    pkt_len = idx;

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    res = _service_mqtt_write(mq_process, pkt, pkt_len, mq_process->send_timeout_ms);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
    if (res < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_free(pkt);
        if (res != RET_SYS_DEPEND_NWK_WRITE_LESSDATA) {
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
            if (mq_process->network_ctx != NULL) {
                mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
            }
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
        }
        return res;
    }

    mq_process->sysdep->service_sysdep_free(pkt);

    return packet_id;
}

static int32_t _service_mqtt_heartbeat(service_mq_process_t *mq_process)
{
    int32_t res = 0;
    uint8_t pingreq_pkt[2] = { SERVICE_MQTT_PINGREQ_PKT_TYPE, 0x00 };

    _service_mqtt_heartbeat_diag0(mq_process);

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    res = _service_mqtt_write(mq_process, pingreq_pkt, 2, mq_process->send_timeout_ms);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
    if (res < RET_SUCCESS) {
        if (res != RET_SYS_DEPEND_NWK_WRITE_LESSDATA) {
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
            if (mq_process->network_ctx != NULL) {
                mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
            }
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
        }
        return res;
    }

    return RET_SUCCESS;
}

static int32_t _service_mqtt_repub(service_mq_process_t *mq_process)
{
    int32_t res = 0;
    uint64_t time_now = 0;
    service_mqtt_pub_node_t *node = NULL;

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.pub_mutex);
    service_list_for_each_entry(node, &mq_process->pub_list, linked_node, service_mqtt_pub_node_t) {
        time_now = mq_process->sysdep->service_sysdep_time();
        if (time_now < node->last_send_time) {
            node->last_send_time = time_now;
        }
        if ((time_now - node->last_send_time) >= mq_process->repub_timeout_ms) {
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
            res = _service_mqtt_write(mq_process, node->packet, node->len, mq_process->send_timeout_ms);
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
            if (res < RET_SUCCESS) {
                mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.pub_mutex);
                if (res != RET_SYS_DEPEND_NWK_WRITE_LESSDATA) {
                    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
                    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
                    if (mq_process->network_ctx != NULL) {
                        mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
                    }
                    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
                    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
                }
                return res;
            }
        }
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.pub_mutex);

    return RET_SUCCESS;
}

static int32_t _service_mqtt_process_datalist_insert(service_mq_process_t *mq_process,
        service_mqtt_process_data_t *process_data)
{
    service_mqtt_process_data_node_t *node = NULL;

    service_list_for_each_entry(node, &mq_process->process_data_list,
                             linked_node, service_mqtt_process_data_node_t) {
        if (node->process_data.ctx == process_data->ctx) {
            node->process_data.context = process_data->context;
            return RET_SUCCESS;
        }
    }

    if (&node->linked_node == &mq_process->process_data_list) {
        node = mq_process->sysdep->service_sysdep_malloc(sizeof(service_mqtt_process_data_node_t));
        if (node == NULL) {
            return RET_SYS_DEPEND_MALLOC_FAILED;
        }
        memset(node, 0, sizeof(service_mqtt_process_data_node_t));
        CORE_INIT_LIST_HEAD(&node->linked_node);
        memcpy(&node->process_data, process_data, sizeof(service_mqtt_process_data_t));

        service_list_add_tail(&node->linked_node, &mq_process->process_data_list);
    }

    return RET_SUCCESS;
}

static void _service_mqtt_process_datalist_remove(service_mq_process_t *mq_process,
        service_mqtt_process_data_t *process_data)
{
    service_mqtt_process_data_node_t *node = NULL;
    service_mqtt_process_data_node_t *next = NULL;

    service_list_for_each_entry_safe(node, next, &mq_process->process_data_list,linked_node, service_mqtt_process_data_node_t) {
        if (node->process_data.ctx == process_data->ctx) {
            service_list_del(&node->linked_node);
            mq_process->sysdep->service_sysdep_free(node);
            return;
        }
    }
}

static void _service_mqtt_process_datalist_destroy(service_mq_process_t *mq_process)
{
    service_mqtt_process_data_node_t *node = NULL, *next = NULL;

    service_list_for_each_entry_safe(node, next, &mq_process->process_data_list,
                                  linked_node, service_mqtt_process_data_node_t) {
        service_list_del(&node->linked_node);
        mq_process->sysdep->service_sysdep_free(node);
    }
}

static int32_t _service_mqtt_append_process_data(service_mq_process_t *mq_process,
        service_mqtt_process_data_t *process_data)
{
    int32_t res = RET_SUCCESS;

    if (process_data->ctx == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.process_ctx_mutex);
    res = _service_mqtt_process_datalist_insert(mq_process, process_data);
    if (res >= RET_SUCCESS && mq_process->cn_flag.tmp_conn == 1) {
        tiot_mqtt_event_t event;

        memset(&event, 0, sizeof(tiot_mqtt_event_t));
        event.type = TIOT_MQTT_EVENT_CONNECT;

        process_data->ctx(process_data->context, &event, NULL);
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.process_ctx_mutex);

    return res;
}

static int32_t _service_mqtt_remove_process_data(service_mq_process_t *mq_process,
        service_mqtt_process_data_t *process_data)
{
    if (process_data == NULL || process_data->ctx == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    int32_t res = RET_SUCCESS;

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.process_ctx_mutex);
    _service_mqtt_process_datalist_remove(mq_process, process_data);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.process_ctx_mutex);

    return res;
}

static void _service_mqtt_process_data_process(service_mq_process_t *mq_process, service_mqtt_event_t *service_event)
{
    service_mqtt_process_data_node_t *node = NULL;

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.process_ctx_mutex);
    service_list_for_each_entry(node, &mq_process->process_data_list,
                             linked_node, service_mqtt_process_data_node_t) {
        node->process_data.ctx(node->process_data.context, NULL, service_event);
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.process_ctx_mutex);
}

static int32_t _service_mqtt_reconnect(service_mq_process_t *mq_process)
{
    int32_t res = RET_SYS_DEPEND_NWK_CLOSED;
    uint64_t time_now = 0;
    uint32_t interval_ms = mq_process->reconnect_params.interval_ms;
    if(mq_process->reconnect_params.backoff_enabled){
        interval_ms = mq_process->reconnect_params.interval_ms * (mq_process->reconnect_params.reconnect_counter + 1) + mq_process->reconnect_params.rand_ms;
    }

    if (mq_process->network_ctx != NULL) {
        return RET_SUCCESS;
    }

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
    time_now = mq_process->sysdep->service_sysdep_time();
    if (time_now < mq_process->reconnect_params.last_retry_time) {
        mq_process->reconnect_params.last_retry_time = time_now;
    }
    if (time_now >= (mq_process->reconnect_params.last_retry_time + interval_ms)) {
        log_debug("MQTT network disconnect, try to reconnecting...\r\n");
        res = _service_mqtt_connect(mq_process, NULL);
        mq_process->reconnect_params.last_retry_time = mq_process->sysdep->service_sysdep_time();
        if(mq_process->reconnect_params.backoff_enabled)
        {
            if(RET_MQTT_CONNECT_SUCCESS == res){
                mq_process->reconnect_params.reconnect_counter = 0;
            }
            else if(mq_process->reconnect_params.reconnect_counter < SERVICE_MQTT_DEFAULT_RECONN_MAX_COUNTERS)
                mq_process->reconnect_params.reconnect_counter++;
        }
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);

    return res;
}

static int32_t _service_mqtt_read_remainlen(service_mq_process_t *mq_process, uint32_t *remainlen)
{
    int32_t res = 0;
    uint8_t ch = 0;
    uint32_t multiplier = 1;
    uint32_t mqtt_remainlen = 0;

    do {
        res = _service_mqtt_read(mq_process, &ch, 1, mq_process->recv_timeout_ms);
        if (res < RET_SUCCESS) {
            return res;
        }
        mqtt_remainlen += (ch & 127) * multiplier;
        if (multiplier > 128 * 128 * 128) {
            return RET_MQTT_MALFORMED_REMAINING_LEN;
        }
        multiplier *= 128;
    } while ((ch & 128) != 0);

    *remainlen = mqtt_remainlen;

    return RET_SUCCESS;
}

static int32_t _service_mqtt_read_remainbytes(service_mq_process_t *mq_process, uint32_t remainlen, uint8_t **output)
{
    int32_t res = 0;
    uint8_t *remain = NULL;

    if (remainlen > 0) {
        remain = mq_process->sysdep->service_sysdep_malloc(remainlen);
        if (remain == NULL) {
            return RET_SYS_DEPEND_MALLOC_FAILED;
        }
        memset(remain, 0, remainlen);

        res = _service_mqtt_read(mq_process, remain, remainlen, mq_process->recv_timeout_ms);
        if (res < RET_SUCCESS) {
            mq_process->sysdep->service_sysdep_free(remain);
            if (res == RET_SYS_DEPEND_NWK_READ_LESSDATA) {
                return RET_MQTT_MALFORMED_REMAINING_BYTES;
            } else {
                return res;
            }
        }
    }
    *output = remain;

    return RET_SUCCESS;
}

static int32_t _service_mqtt_pingresp_ctx(service_mq_process_t *mq_process, uint8_t *input, uint32_t len)
{
    tiot_mqtt_recv_t packet;
    uint64_t rtt = mq_process->sysdep->service_sysdep_time()  \
                   - mq_process->heartbeat_params.last_send_time;

    if (len != 0) {
        return RET_MQTT_RECV_INVALID_PINRESP_PACKET;
    }

    /* heartbreat rtt stats */
    if (rtt < SERVICE_MQTT_NWKSTATS_RTT_THRESHOLD) {
        mq_process->nwkstats_info.rtt = (mq_process->nwkstats_info.rtt + rtt) / 2;
    }

    memset(&packet, 0, sizeof(tiot_mqtt_recv_t));

    if (mq_process->recv_ctx != NULL) {
        packet.type = TIOT_MQTTRECV_HEARTBEAT_RESPONSE;
        mq_process->recv_ctx((void *)mq_process, &packet, mq_process->userdata);
    }

    _service_mqtt_heartbeat_diag1(mq_process);

    return RET_SUCCESS;
}

static int32_t _service_mqtt_puback_send(service_mq_process_t *mq_process, uint16_t packet_id)
{
    int32_t res = 0;
    uint8_t pkt[4] = {0};

    /* Packet Type */
    pkt[0] = SERVICE_MQTT_PUBACK_PKT_TYPE;

    /* Remaining Lengh */
    pkt[1] = 2;

    /* Packet Id */
    pkt[2] = (uint16_t)((packet_id >> 8) & 0x00FF);
    pkt[3] = (uint16_t)((packet_id) & 0x00FF);

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    res = _service_mqtt_write(mq_process, pkt, 4, mq_process->send_timeout_ms);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
    if (res < RET_SUCCESS) {
        if (res != RET_SYS_DEPEND_NWK_WRITE_LESSDATA) {
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
            if (mq_process->network_ctx != NULL) {
                mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
            }
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
        }
        return res;
    }

    return RET_SUCCESS;
}

static int32_t _service_mqtt_topic_compare(char *topic, uint32_t topic_len, char *cmp_topic, uint32_t cmp_topic_len)
{
    uint32_t idx = 0, cmp_idx = 0;

    for (idx = 0, cmp_idx = 0; idx < topic_len; idx++) {
        /* compare topic alreay out of bounds */
        if (cmp_idx >= cmp_topic_len) {
            /* compare success only in case of the left string of topic is "/#" */
            if ((topic_len - idx == 2) && (memcmp(&topic[idx], "/#", 2) == 0)) {
                return RET_SUCCESS;
            } else {
                return RET_MQTT_TOPIC_COMPARE_FAILED;
            }
        }

        /* if topic reach the '#', compare success */
        if (topic[idx] == '#') {
            return RET_SUCCESS;
        }

        if (topic[idx] == '+') {
            /* wildcard + exist */
            for (; cmp_idx < cmp_topic_len; cmp_idx++) {
                if (cmp_topic[cmp_idx] == '/') {
                    /* if topic already reach the bound, compare topic should not contain '/' */
                    if (idx + 1 == topic_len) {
                        return RET_MQTT_TOPIC_COMPARE_FAILED;
                    } else {
                        break;
                    }
                }
            }
        } else {
            /* compare each character */
            if (topic[idx] != cmp_topic[cmp_idx]) {
                return RET_MQTT_TOPIC_COMPARE_FAILED;
            }
            cmp_idx++;
        }
    }

    /* compare topic should be reach the end */
    if (cmp_idx < cmp_topic_len) {
        return RET_MQTT_TOPIC_COMPARE_FAILED;
    }
    return RET_SUCCESS;
}

static void _service_mq_processrlist_append(service_mq_process_t *mq_process, struct service_list_head *dest,
        struct service_list_head *src, uint8_t *found)
{
    service_mqtt_sub_ctx_node_t *node = NULL, *copy_node = NULL;

    service_list_for_each_entry(node, src, linked_node, service_mqtt_sub_ctx_node_t) {
        copy_node = mq_process->sysdep->service_sysdep_malloc(sizeof(service_mqtt_sub_ctx_node_t));
        if (copy_node == NULL) {
            continue;
        }
        memset(copy_node, 0, sizeof(service_mqtt_sub_ctx_node_t));
        CORE_INIT_LIST_HEAD(&copy_node->linked_node);
        copy_node->ctx = node->ctx;
        copy_node->userdata = node->userdata;

        service_list_add_tail(&copy_node->linked_node, dest);
        *found = 1;
    }
}

static void _service_mq_processrlist_destroy(service_mq_process_t *mq_process, struct service_list_head *list)
{
    service_mqtt_sub_ctx_node_t *node = NULL, *next = NULL;

    service_list_for_each_entry_safe(node, next, list, linked_node, service_mqtt_sub_ctx_node_t) {
        service_list_del(&node->linked_node);
        mq_process->sysdep->service_sysdep_free(node);
    }
}

static int32_t _service_mqtt_parse_pub_properties(service_mq_process_t *mq_process, uint8_t *property,
        pub_property_t *pub_prop, uint32_t *parsed_property_len, uint8_t *parsed_offset, uint32_t remain_len)
{
    general_property_t general_prop = {0};
    int32_t res = _service_mqtt_parse_general_properties(mq_process, property, &general_prop, parsed_property_len,
                  parsed_offset, remain_len);
    if (res != RET_SUCCESS) {
        return res;
    }

    pub_prop->message_expire_interval = general_prop.message_expire_interval;
    pub_prop->topic_alias = general_prop.topic_alias;
    memcpy(&(pub_prop->user_property[0]), &(general_prop.user_property[0]),
           MQTT5_ATTRIBUTE_MAX * (sizeof(mqtt5_property_t *)));
    pub_prop->subscription_identifier = general_prop.subscription_identifier;
    pub_prop->response_topic = general_prop.response_topic;
    pub_prop->correlation_data = general_prop.correlation_data;
    return RET_SUCCESS;
}

uint8_t _service_mqtt_process_topic_alias(service_mq_process_t *mq_process, pub_property_t *pub_prop, uint32_t topic_len,
                                       char *topic,
                                       service_mqtt_topic_alias_node_t *alias_node)
{
    service_mqtt_topic_alias_node_t *topic_alias_node = NULL;

    uint8_t use_alias_topic = 0;
    if (NULL != pub_prop && pub_prop->topic_alias > 0) {
        if (topic_len > 0) {
            /* A topic appears first time during this connection. */
            service_mqtt_buff_t alias_topic = {
                .buffer = (uint8_t *)topic,
                .len = topic_len,
            };
            /* Insert it into topic alias list */
            _service_mqtt_topic_alias_list_insert(mq_process, &alias_topic, pub_prop->topic_alias,
                                               &(mq_process->rx_topic_alias_list));
        } else {
            /* topic len is 0. User previos topic alias */
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->topic_alias_mutex);
            service_list_for_each_entry(topic_alias_node, &mq_process->rx_topic_alias_list, linked_node,
                                     service_mqtt_topic_alias_node_t) {
                if (topic_alias_node->topic_alias == pub_prop->topic_alias) {
                    log_debug("found a rx topic alias\n");
                    use_alias_topic = 1;
                    memcpy(alias_node, topic_alias_node, sizeof(service_mqtt_topic_alias_node_t));
                    break;
                }
            }
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->topic_alias_mutex);
        }
    }
    return use_alias_topic;
}

static int32_t _service_mqtt_pub_ctx(service_mq_process_t *mq_process, uint8_t *input, uint32_t len, uint8_t qos,
                                      uint32_t remain_len)
{
    uint8_t sub_found = 0;
    uint32_t idx = 0;
    uint16_t packet_id = 0;
    uint16_t utf8_strlen = 0;
    tiot_mqtt_recv_t packet;
    uint32_t topic_len = 0;
    void *userdata;
    service_mqtt_sub_node_t *sub_node = NULL;
    service_mqtt_topic_alias_node_t topic_alias_node = {0};

    service_mqtt_sub_ctx_node_t *ctx_node = NULL;
    struct service_list_head ctx_list_copy;
    uint8_t use_alias_topic  = 0;
    pub_property_t pub_prop = {0};
    uint32_t total_prop_len = 0;

    if (input == NULL || len == 0 || qos > SERVICE_MQTT_QOS1) {
        return RET_MQTT_RECV_INVALID_PUBLISH_PACKET;
    }

    memset(&packet, 0, sizeof(tiot_mqtt_recv_t));

    packet.data.pub.qos = qos;
    packet.type = TIOT_MQTTRECV_PUB;

    /* Topic Length */
    if (0 == _service_mqtt_5_feature_is_enabled(mq_process) && len < 2) {
        return RET_MQTT_RECV_INVALID_PUBLISH_PACKET;
    }

    utf8_strlen = input[idx++] << 8;
    utf8_strlen |= input[idx++];

    packet.data.pub.topic = (char *)&input[idx];
    packet.data.pub.topic_len = utf8_strlen;
    idx += utf8_strlen;

    /* Packet Id For QOS1 */
    if (len < idx) {
        return RET_MQTT_RECV_INVALID_PUBLISH_PACKET;
    }
    if (qos == SERVICE_MQTT_QOS1) {
        if (len < idx + 2) {
            return RET_MQTT_RECV_INVALID_PUBLISH_PACKET;
        }
        packet_id = input[idx++] << 8;
        packet_id |= input[idx++];

    }

    if (_service_mqtt_5_feature_is_enabled(mq_process)) {
        uint32_t property_len = 0;
        uint8_t offset = 0;

        int32_t res = _service_mqtt_parse_pub_properties(mq_process, &input[idx], &pub_prop, &property_len, &offset, remain_len);
        if (res < RET_SUCCESS) {
            return res;
        }

        total_prop_len = property_len + offset ;
        idx += total_prop_len;

        packet.data.pub.pub_prop = &pub_prop;
        use_alias_topic = _service_mqtt_process_topic_alias(mq_process, &pub_prop, packet.data.pub.topic_len,
                          packet.data.pub.topic, &topic_alias_node);
    }

    /* Payload Len */
    if ((int64_t)len - SERVICE_MQTT_PUBLISH_TOPICLEN_LEN - (int64_t)packet.data.pub.topic_len < 0) {
        return RET_MQTT_RECV_INVALID_PUBLISH_PACKET;
    }
    packet.data.pub.payload = &input[idx];
    packet.data.pub.payload_len = len - SERVICE_MQTT_PUBLISH_TOPICLEN_LEN - packet.data.pub.topic_len - total_prop_len;

    if (qos == SERVICE_MQTT_QOS1) {
        packet.data.pub.payload_len -= SERVICE_MQTT_PACKETID_LEN;
    }

    /* Publish Ack For QOS1 */
    if (qos == SERVICE_MQTT_QOS1) {
        _service_mqtt_puback_send(mq_process, packet_id);
    }

    /* User previos topic alias */
    if (_service_mqtt_5_feature_is_enabled(mq_process) && 1 == use_alias_topic) {
        packet.data.pub.topic = topic_alias_node.topic;
        packet.data.pub.topic_len = strlen(packet.data.pub.topic);
    }

    /* debug */
    topic_len = (uint32_t)packet.data.pub.topic_len;
    log_debug("pub: %.*s\r\n", topic_len, packet.data.pub.topic);

    /* Search Packet Handler In sublist */
    CORE_INIT_LIST_HEAD(&ctx_list_copy);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.sub_mutex);
    service_list_for_each_entry(sub_node, &mq_process->sub_list, linked_node, service_mqtt_sub_node_t) {
        if (_service_mqtt_topic_compare(sub_node->topic, (uint32_t)(strlen(sub_node->topic)), packet.data.pub.topic,
                                     packet.data.pub.topic_len) == RET_SUCCESS) {
            _service_mq_processrlist_append(mq_process, &ctx_list_copy, &sub_node->ctx_list, &sub_found);
        }
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.sub_mutex);

    service_list_for_each_entry(ctx_node, &ctx_list_copy,
                             linked_node, service_mqtt_sub_ctx_node_t) {
        userdata = (ctx_node->userdata == NULL) ? (mq_process->userdata) : (ctx_node->userdata);
        ctx_node->ctx(mq_process, &packet, userdata);
    }
    _service_mq_processrlist_destroy(mq_process, &ctx_list_copy);

    /* User Data Default Packet Handler */
    if (mq_process->recv_ctx && sub_found == 0) {
        mq_process->recv_ctx((void *)mq_process, &packet, mq_process->userdata);
    }

    /* Reclaims Mem of User Properties */
    if (_service_mqtt_5_feature_is_enabled(mq_process)) {
        _service_user_properties(mq_process, pub_prop.user_property);
    }

    return RET_SUCCESS;
}


void _service_mqtt_flow_control_inc(service_mq_process_t *mq_process)
{
    if (_service_mqtt_5_feature_is_enabled(mq_process) && mq_process->flow_control_enabled) {
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.pub_mutex);
        mq_process->server_receive_max++;
        mq_process->server_receive_max = (mq_process->server_receive_max > SERVICE_DEFAULT_SERVER_RECEIVE_MAX) ?
                                          SERVICE_DEFAULT_SERVER_RECEIVE_MAX : mq_process->server_receive_max;
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.pub_mutex);
    }
}

static int32_t _service_mqtt_puback_ctx(service_mq_process_t *mq_process, uint8_t *input, uint32_t len)
{
    tiot_mqtt_recv_t packet;

    /* even maltform puback received, flow control should be re-calculated as well */
    _service_mqtt_flow_control_inc(mq_process);

    if (input == NULL) {
        return RET_MQTT_RECV_INVALID_PUBACK_PACKET;
    }

    if (0 == _service_mqtt_5_feature_is_enabled(mq_process) && len != 2) {
        return RET_MQTT_RECV_INVALID_PUBACK_PACKET;
    } else if (_service_mqtt_5_feature_is_enabled(mq_process) && len < 2) {
        return RET_MQTT_RECV_INVALID_PUBACK_PACKET;
    }

    memset(&packet, 0, sizeof(tiot_mqtt_recv_t));
    packet.type = TIOT_MQTTRECV_PUB_ACK;

    /* Packet Id */
    packet.data.pub_ack.packet_id = input[0] << 8;
    packet.data.pub_ack.packet_id |= input[1];

    /* Remove Packet From republist */
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.pub_mutex);
    _service_mqtt_publist_remove(mq_process, packet.data.pub_ack.packet_id);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.pub_mutex);

    /* User Ctrl Packet Handler */
    if (mq_process->recv_ctx) {
        mq_process->recv_ctx((void *)mq_process, &packet, mq_process->userdata);
    }

    return RET_SUCCESS;
}

static int32_t _service_mqtt_server_disconnect_ctx(service_mq_process_t *mq_process, uint8_t *input, uint32_t len)
{
    tiot_mqtt_recv_t packet;
    uint8_t reason_code;

    if (input == NULL) {
        return RET_MQTT_RECV_INVALID_SERVER_DISCONNECT_PACKET;
    }

    if (_service_mqtt_5_feature_is_enabled(mq_process) && len < 2) {
        return RET_MQTT_RECV_INVALID_SERVER_DISCONNECT_PACKET;
    }

    memset(&packet, 0, sizeof(tiot_mqtt_recv_t));
    packet.type = TIOT_MQTTRECV_DISCONNECT;

    reason_code = input[0];
    packet.data.server_disconnect.reason_code = reason_code;

    /* User Ctrl Packet Handler */
    if (mq_process->recv_ctx) {
        mq_process->recv_ctx((void *)mq_process, &packet, mq_process->userdata);
    }

    /* close socket connect with mqtt broker */
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
    if (mq_process->network_ctx != NULL) {
        mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);

    _service_mqtt_disconnect_event_notify(mq_process, TIOT_MQTTDISCONNEVT_NETWORK_DISCONNECT);
    log_debug("MQTT receives server disconnect, disconnect\r\n");

    return RET_SUCCESS;
}

static void _service_mqtt_subunsuback_ctx(service_mq_process_t *mq_process, uint8_t *input, uint32_t len,
        uint8_t packet_type)
{
    uint32_t idx = 0;
    tiot_mqtt_recv_t packet;
    int32_t res = RET_SUCCESS;

    if (0 == _service_mqtt_5_feature_is_enabled(mq_process)) {
        if (input == NULL || len == 0 ||
            (packet_type == SERVICE_MQTT_SUBACK_PKT_TYPE && len % 3 != 0) ||
            (packet_type == SERVICE_MQTT_UNSUBACK_PKT_TYPE && len % 2 != 0)) {
            return;
        }

        for (idx = 0; idx < len;) {
            memset(&packet, 0, sizeof(tiot_mqtt_recv_t));
            if (packet_type == SERVICE_MQTT_SUBACK_PKT_TYPE) {
                packet.type = TIOT_MQTTRECV_SUB_ACK;
                packet.data.sub_ack.packet_id = input[idx] << 8;
                packet.data.sub_ack.packet_id |= input[idx + 1];
            } else {
                packet.type = TIOT_MQTTRECV_UNSUB_ACK;
                packet.data.unsub_ack.packet_id = input[idx] << 8;
                packet.data.unsub_ack.packet_id |= input[idx + 1];
            }

            if (packet_type == SERVICE_MQTT_SUBACK_PKT_TYPE) {
                if (input[idx + 2] == SERVICE_MQTT_SUBACK_RCODE_MAXQOS0 ||
                    input[idx + 2] == SERVICE_MQTT_SUBACK_RCODE_MAXQOS1 ||
                    input[idx + 2] == SERVICE_MQTT_SUBACK_RCODE_MAXQOS2) {
                    packet.data.sub_ack.res = RET_SUCCESS;
                    packet.data.sub_ack.max_qos = input[idx + 2];
                } else if (input[idx + 2] == SERVICE_MQTT_SUBACK_RCODE_FAILURE) {
                    packet.data.sub_ack.res = RET_MQTT_SUBACK_RCODE_FAILURE;
                } else {
                    packet.data.sub_ack.res = RET_MQTT_SUBACK_RCODE_UNKNOWN;
                }
                idx += 3;
            } else {
                idx += 2;
            }

            if (mq_process->recv_ctx) {
                mq_process->recv_ctx((void *)mq_process, &packet, mq_process->userdata);
            }
        }
    } else {
        uint32_t idx = 0;
        uint32_t property_len = 0;
        uint8_t offset = 0;

        if (input == NULL || len == 0) {
            return;
        }

        /* Packet ID */
        memset(&packet, 0, sizeof(tiot_mqtt_recv_t));
        if (packet_type == SERVICE_MQTT_SUBACK_PKT_TYPE) {
            packet.type = TIOT_MQTTRECV_SUB_ACK;
            packet.data.sub_ack.packet_id = input[idx] << 8;
            packet.data.sub_ack.packet_id |= input[idx + 1];
        } else {
            packet.type = TIOT_MQTTRECV_UNSUB_ACK;
            packet.data.unsub_ack.packet_id = input[idx] << 8;
            packet.data.unsub_ack.packet_id |= input[idx + 1];
        }
        idx += 2;

        /* Properties */
        res = _read_variable_byte_interger(&(input[idx]), &property_len,  &offset);
        if (res < RET_SUCCESS) {
            return;
        }
        idx = idx + offset + property_len;

        /* Suback Flags */
        if (packet_type == SERVICE_MQTT_SUBACK_PKT_TYPE) {
            if (input[idx] == SERVICE_MQTT_SUBACK_RCODE_MAXQOS0 ||
                input[idx] == SERVICE_MQTT_SUBACK_RCODE_MAXQOS1 ||
                input[idx] == SERVICE_MQTT_SUBACK_RCODE_MAXQOS2) {
                packet.data.sub_ack.res = RET_SUCCESS;
                packet.data.sub_ack.max_qos = input[idx];
            } else if (input[idx] == SERVICE_MQTT_SUBACK_RCODE_FAILURE) {
                packet.data.sub_ack.res = RET_MQTT_SUBACK_RCODE_FAILURE;
            } else {
                packet.data.sub_ack.res = RET_MQTT_SUBACK_RCODE_UNKNOWN;
            }
        }

        if (mq_process->recv_ctx) {
            mq_process->recv_ctx((void *)mq_process, &packet, mq_process->userdata);
        }
    }

}

int32_t service_mqtt_setopt(void *ctx, service_mqtt_option_t option, void *data)
{
    int32_t res = 0;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (option >= SERVICE_MQTTOPT_MAX) {
        return RET_USER_INPUT_OUT_RANGE;
    }

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    switch (option) {
        case SERVICE_MQTTOPT_APPEND_PROCESS_HANDLER: {
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
            res = _service_mqtt_append_process_data(mq_process, (service_mqtt_process_data_t *)data);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
        }
        break;
        case SERVICE_MQTTOPT_REMOVE_PROCESS_HANDLER: {
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
            res = _service_mqtt_remove_process_data(mq_process, (service_mqtt_process_data_t *)data);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
        }
        break;
        default: {
            res = RET_USER_INPUT_UNKNOWN_OPTION;
        }
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);

    return res;
}

void *tiot_mqtt_init(void)
{
    int32_t res = RET_SUCCESS;
    uint32_t rand_value = 0;
    service_mq_process_t *mq_process = NULL;
    tiot_sysdep_portfile_t *sysdep = NULL;

    sysdep = tiot_sysdep_get_global_config();
    if (sysdep == NULL) {
        return NULL;
    }

    res = service_global_init(sysdep);
    if (res < RET_SUCCESS) {
        return NULL;
    }

    mq_process = sysdep->service_sysdep_malloc(sizeof(service_mq_process_t));
    if (mq_process == NULL) {
        service_global_deinit(sysdep);
        return NULL;
    }
    sysdep->service_sysdep_rand((uint8_t*)&rand_value, sizeof(rand_value));
    memset(mq_process, 0, sizeof(service_mq_process_t));

    mq_process->sysdep = sysdep;
    mq_process->keep_alive_s = SERVICE_MQTT_DEFAULT_KEEPALIVE_S;
    mq_process->clean_session = SERVICE_MQTT_DEFAULT_CLEAN_SESSION;
    mq_process->connect_timeout_ms = SERVICE_MQTT_DEFAULT_CONNECT_TIMEOUT_MS;
    mq_process->heartbeat_params.interval_ms = SERVICE_MQTT_DEFAULT_HEARTBEAT_INTERVAL_MS;
    mq_process->heartbeat_params.max_lost_times = SERVICE_MQTT_DEFAULT_HEARTBEAT_MAX_LOST_TIMES;
    mq_process->reconnect_params.enabled = SERVICE_MQTT_DEFAULT_RECONN_ENABLED;
    mq_process->reconnect_params.interval_ms = SERVICE_MQTT_DEFAULT_RECONN_INTERVAL_MS;
    mq_process->reconnect_params.backoff_enabled = 1;
    mq_process->reconnect_params.rand_ms = rand_value % SERVICE_MQTT_DEFAULT_RECONN_RANDLIMIT_MS - SERVICE_MQTT_DEFAULT_RECONN_RANDLIMIT_MS / 2;
    mq_process->reconnect_params.reconnect_counter = 0;
    mq_process->send_timeout_ms = SERVICE_MQTT_DEFAULT_SEND_TIMEOUT_MS;
    mq_process->recv_timeout_ms = SERVICE_MQTT_DEFAULT_RECV_TIMEOUT_MS;
    mq_process->repub_timeout_ms = SERVICE_MQTT_DEFAULT_REPUB_TIMEOUT_MS;
    mq_process->deinit_timeout_ms = SERVICE_MQTT_DEFAULT_DEINIT_TIMEOUT_MS;

    mq_process->all_mutex.data_mutex = sysdep->service_sysdep_mutex_init();
    mq_process->all_mutex.send_mutex = sysdep->service_sysdep_mutex_init();
    mq_process->all_mutex.recv_mutex = sysdep->service_sysdep_mutex_init();
    mq_process->all_mutex.sub_mutex = sysdep->service_sysdep_mutex_init();
    mq_process->all_mutex.pub_mutex = sysdep->service_sysdep_mutex_init();
    mq_process->topic_alias_mutex = sysdep->service_sysdep_mutex_init();
    mq_process->all_mutex.process_ctx_mutex = sysdep->service_sysdep_mutex_init();
    mq_process->protocol_version = TIOT_MQTT_VERSION_3_1;
    mq_process->tx_packet_max_size = SERVICE_TX_PKT_MAX_LENGTH;

    CORE_INIT_LIST_HEAD(&mq_process->sub_list);
    CORE_INIT_LIST_HEAD(&mq_process->pub_list);
    CORE_INIT_LIST_HEAD(&mq_process->process_data_list);
    CORE_INIT_LIST_HEAD(&mq_process->rx_topic_alias_list);
    CORE_INIT_LIST_HEAD(&mq_process->tx_topic_alias_list);

    mq_process->exec_enabled = 1;
    mq_process->server_receive_max = SERVICE_DEFAULT_SERVER_RECEIVE_MAX;

    return mq_process;
}

/* set host value */
int32_t tiot_mqtt_set_host(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    res = service_strdup(mq_process->sysdep, &mq_process->host, (char *)data);
    log_debug("TIOT_MQTTOPT_HOST host: %s \r\n", mq_process->host);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);

    _service_mqtt_exec_dec(mq_process);
    return 0;
}

/* set port value */
int32_t tiot_mqtt_set_port(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    mq_process->port = *(uint16_t *)data;
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set product_key value */
int32_t tiot_mqtt_set_product_key(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    res = service_strdup(mq_process->sysdep, &mq_process->product_key, (char *)data);
    _service_mqtt_sign_clean(mq_process);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set device_name value */
int32_t tiot_mqtt_set_device_name(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    res = service_strdup(mq_process->sysdep, &mq_process->device_name, (char *)data);
    _service_mqtt_sign_clean(mq_process);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set device secret value */
int32_t tiot_mqtt_set_device_secret(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    res = service_strdup(mq_process->sysdep, &mq_process->device_secret, (char *)data);
    _service_mqtt_sign_clean(mq_process);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set extend clientid value */
int32_t tiot_mqtt_set_extend_clientid(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    res = service_strdup(mq_process->sysdep, &mq_process->extend_clientid, (char *)data);
    _service_mqtt_sign_clean(mq_process);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}


/* set security_mode */
int32_t tiot_mqtt_set_security_mode(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    res = service_strdup(mq_process->sysdep, &mq_process->security_mode, (char *)data);
    _service_mqtt_sign_clean(mq_process);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set user name */
int32_t tiot_mqtt_set_user_name(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    res = service_strdup(mq_process->sysdep, &mq_process->username, (char *)data);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set password */
int32_t tiot_mqtt_set_password(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    res = service_strdup(mq_process->sysdep, &mq_process->password, (char *)data);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}


/* set clientid */
int32_t tiot_mqtt_set_clientid(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    res = service_strdup(mq_process->sysdep, &mq_process->clientid, (char *)data);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set keepalive_sec */
int32_t tiot_mqtt_keepalive_sec(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->keep_alive_s = *(uint16_t *)data;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set clean_session */
int32_t tiot_mqtt_clean_session(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    if (*(uint8_t *)data != 0 && *(uint8_t *)data != 1)
    {
        res = RET_USER_INPUT_OUT_RANGE;
    }
    mq_process->clean_session = *(uint8_t *)data;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}


/* set network cred */
int32_t tiot_mqtt_network_cred(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    if (mq_process->cred != NULL)
    {
        mq_process->sysdep->service_sysdep_free(mq_process->cred);
        mq_process->cred = NULL;
    }
    mq_process->cred = mq_process->sysdep->service_sysdep_malloc(sizeof(tiot_sysdep_network_cred_t));
    if (mq_process->cred != NULL)
    {
        memset(mq_process->cred, 0, sizeof(tiot_sysdep_network_cred_t));
        memcpy(mq_process->cred, data, sizeof(tiot_sysdep_network_cred_t));
    }
    else
    {
        res = RET_SYS_DEPEND_MALLOC_FAILED;
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set timeout ms */
int32_t tiot_mqtt_timeout_ms(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }
    
    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

     mq_process->connect_timeout_ms = *(uint32_t *)data;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set heartbeat_interval_ms */
int32_t tiot_mqtt_heartbeat_interval_ms(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->heartbeat_params.interval_ms = *(uint32_t *)data;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set heartbeat_max_lost */
int32_t tiot_mqtt_heartbeat_max_lost(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->heartbeat_params.max_lost_times = *(uint8_t *)data;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}


/* set reconn_enabled */
int32_t tiot_mqtt_reconn_enabled(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    if (*(uint8_t *)data != 0 && *(uint8_t *)data != 1)
    {
        res = RET_USER_INPUT_OUT_RANGE;
    }
    mq_process->reconnect_params.enabled = *(uint8_t *)data;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set reconn_interval_ms */
int32_t tiot_mqtt_reconn_interval_ms(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->reconnect_params.interval_ms = *(uint32_t *)data;
    mq_process->reconnect_params.backoff_enabled = 0;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set send_timeout_ms */
int32_t tiot_mqtt_send_timeout_ms(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->send_timeout_ms = *(uint32_t *)data;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set recv_timeout_ms */
int32_t tiot_mqtt_recv_timeout_ms(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->recv_timeout_ms = *(uint32_t *)data;
    
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set repub_timeout_ms */
int32_t tiot_mqtt_repub_timeout_ms(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->repub_timeout_ms = *(uint32_t *)data;
        
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set deinit_timeout_ms */
int32_t tiot_mqtt_deinit_timeout_ms(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->deinit_timeout_ms = *(uint32_t *)data;
            
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set recv_ctx */
int32_t tiot_mqtt_recv_ctx(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->recv_ctx = (tiot_mqtt_recv_ctx_t)data;
             
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}


/* set event_ctx */
int32_t tiot_mqtt_event_ctx(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->event_ctx = (tiot_mqtt_event_ctx_t)data;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set append_topic_map */
int32_t tiot_mqtt_append_topic_map(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    res = _service_mqtt_append_topic_map(mq_process, (tiot_mqtt_topic_map_t *)data);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}


/* set remove_topic_map */
int32_t tiot_mqtt_remove_topic_map(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    res = _service_mqtt_remove_topic_map(mq_process, (tiot_mqtt_topic_map_t *)data);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set append_requestid */
int32_t tiot_mqtt_append_requestid(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->append_requestid = *(uint8_t *)data;
        
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set append_userdata */
int32_t tiot_mqtt_userdata(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);

    mq_process->userdata = data;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}


/* set version */
int32_t tiot_mqtt_version(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    uint8_t version;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    version = *(uint8_t *)data;
    mq_process->protocol_version = version;

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

/* set assigned_clientid */
int32_t tiot_mqtt_assigned_clientid(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    uint8_t use_assigned_clientid;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    use_assigned_clientid = *(uint8_t *)data;
    mq_process->use_assigned_clientid = use_assigned_clientid;
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}


/* set flow_control_enabled */
int32_t tiot_mqtt_flow_control_enabled(void *ctx, void *data)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || data == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.data_mutex);
    mq_process->flow_control_enabled = *(uint8_t *)data;
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.data_mutex);
    _service_mqtt_exec_dec(mq_process);

    return 0;
}

int32_t tiot_mqtt_deinit(void **ctx)
{
    uint64_t deinit_timestart = 0;
    service_mq_process_t *mq_process = NULL;
    service_mqtt_event_t service_event;

    if (ctx == NULL || *ctx == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    mq_process = *(service_mq_process_t **)ctx;

    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    mq_process->exec_enabled = 0;
    deinit_timestart = mq_process->sysdep->service_sysdep_time();
    do {
        if (mq_process->exec_count == 0) {
            break;
        }
        mq_process->sysdep->service_sysdep_sleep(SERVICE_MQTT_DEINIT_INTERVAL_MS);
    } while ((mq_process->sysdep->service_sysdep_time() - deinit_timestart) < mq_process->deinit_timeout_ms);

    if (mq_process->exec_count != 0) {
        return RET_MQTT_DEINIT_TIMEOUT;
    }

    _service_mqtt_topic_alias_list_remove_all(mq_process);

    memset(&service_event, 0, sizeof(service_mqtt_event_t));
    service_event.type = CORE_MQTTEVT_DEINIT;
    _service_mqtt_process_data_process(mq_process, &service_event);

    *ctx = NULL;

    if (mq_process->network_ctx != NULL) {
        mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
    }
    if (mq_process->host != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->host);
    }
    if (mq_process->product_key != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->product_key);
    }
    if (mq_process->device_name != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->device_name);
    }
    if (mq_process->device_secret != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->device_secret);
    }
    if (mq_process->username != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->username);
    }
    if (mq_process->password != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->password);
    }
    if (mq_process->clientid != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->clientid);
    }
    if (mq_process->extend_clientid != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->extend_clientid);
    }
    if (mq_process->security_mode != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->security_mode);
    }
    if (mq_process->cred != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->cred);
    }
    if (mq_process->pre_connect_property != NULL) {
        mq_process->sysdep->service_sysdep_free(mq_process->pre_connect_property);
    }

    mq_process->sysdep->service_sysdep_mutex_deinit(&mq_process->all_mutex.data_mutex);
    mq_process->sysdep->service_sysdep_mutex_deinit(&mq_process->all_mutex.send_mutex);
    mq_process->sysdep->service_sysdep_mutex_deinit(&mq_process->all_mutex.recv_mutex);
    mq_process->sysdep->service_sysdep_mutex_deinit(&mq_process->all_mutex.sub_mutex);
    mq_process->sysdep->service_sysdep_mutex_deinit(&mq_process->topic_alias_mutex);
    mq_process->sysdep->service_sysdep_mutex_deinit(&mq_process->all_mutex.pub_mutex);
    mq_process->sysdep->service_sysdep_mutex_deinit(&mq_process->all_mutex.process_ctx_mutex);

    _service_mqtt_sublist_destroy(mq_process);
    _service_mqtt_publist_destroy(mq_process);
    _service_mqtt_process_datalist_destroy(mq_process);

    service_global_deinit(mq_process->sysdep);

    mq_process->sysdep->service_sysdep_free(mq_process);

    return RET_SUCCESS;
}

static int32_t _mqtt_connect_with_prop(void *ctx, conn_property_t *connect_property)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;
    uint64_t time_ent_ms = 0;

    if (mq_process == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    time_ent_ms = mq_process->sysdep->service_sysdep_time();

    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);

    mq_process->cn_flag.disconn_called = 0;

    /* if network connection exist, close first */
    log_debug("MQTT user calls tiot_mqtt_connect api, connect\r\n");
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);

    res = _service_mqtt_connect(mq_process, connect_property);

    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);

    if (res == RET_MQTT_CONNECT_SUCCESS) {
        uint64_t time_ms = mq_process->sysdep->service_sysdep_time();
        uint32_t time_delta = (uint32_t)(time_ms - time_ent_ms);

        log_info("MQTT connect success in %d ms\r\n", time_delta);
        _service_mqtt_connect_event_notify(mq_process);
        res = RET_SUCCESS;
    } else {
        _service_mqtt_disconnect_event_notify(mq_process, TIOT_MQTTDISCONNEVT_NETWORK_DISCONNECT);
    }

    _service_mqtt_exec_dec(mq_process);

    return res;
}

int32_t tiot_mqtt_connect(void *ctx)
{
    return _mqtt_connect_with_prop(ctx, NULL);
}

int32_t tiot_mqtt_connect_with_prop(void *ctx, conn_property_t *connect_property)
{
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (0 == _service_mqtt_5_feature_is_enabled(mq_process)) {
        return RET_MQTT_INVALID_PROTOCOL_VERSION;
    }

    /* clear previous connection properties */
    if (NULL != mq_process->pre_connect_property) {
        mq_process->sysdep->service_sysdep_free(mq_process->pre_connect_property);
        mq_process->pre_connect_property = NULL;
    }
    return _mqtt_connect_with_prop(ctx, connect_property);
}

int32_t tiot_mqtt_disconnect(void *ctx)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);

    mq_process->cn_flag.disconn_called = 1;

    /* send mqtt disconnect packet to mqtt broker first */
    _service_mqtt_disconnect(ctx);

    /* close socket connect with mqtt broker */
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
    if (mq_process->network_ctx != NULL) {
        mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);

    _service_mqtt_disconnect_event_notify(mq_process, TIOT_MQTTDISCONNEVT_NETWORK_DISCONNECT);
    log_debug("MQTT user calls tiot_mqtt_disconnect api, disconnect\r\n");

    _service_mqtt_exec_dec(mq_process);

    return res;
}

int32_t tiot_mqtt_heartbeat(void *ctx)
{
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    return _service_mqtt_heartbeat(mq_process);
}

int32_t tiot_mqtt_process(void *ctx)
{
    int32_t res = RET_SUCCESS;
    uint64_t time_now = 0;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);

    /* mqtt PINREQ packet */
    time_now = mq_process->sysdep->service_sysdep_time();
    if (time_now < mq_process->heartbeat_params.last_send_time) {
        mq_process->heartbeat_params.last_send_time = time_now;
    }
    if ((time_now - mq_process->heartbeat_params.last_send_time) >= mq_process->heartbeat_params.interval_ms) {
        res = _service_mqtt_heartbeat(mq_process);
        if (res == RET_SUCCESS) {
            mq_process->heartbeat_params.last_send_time = time_now;
            mq_process->heartbeat_params.lost_times++;
        }
    }

    /* mqtt QoS1 packet republish */
    _service_mqtt_repub(mq_process);

    /* mqtt process ctx process */
    _service_mqtt_process_data_process(mq_process, NULL);

    _service_mqtt_exec_dec(mq_process);

    return res;
}

uint32_t _service_get_general_property_len(general_property_t *general_prop)
{
    uint32_t property_len = 0;
    uint8_t subscription_identifier[4] = {0};
    uint32_t subscription_identifer_offset = 0;
    mqtt5_property_t **list = general_prop->user_property;
    uint8_t iter = 0;

    if (general_prop->topic_alias > 0) {
        /* Topic talkweb */
        property_len += SERVICE_MQTT_V5_PROPERTY_ID_LEN + SERVICE_MQTT_V5_TOPIC_ALIAS_LEN;
    }

    if (general_prop->topic_alias_max > 0) {
        property_len += SERVICE_MQTT_V5_PROPERTY_ID_LEN + SERVICE_MQTT_V5_TOPIC_ALIAS_MAX_LEN;
    }

    /* receive max */
    if (general_prop->client_receive_max > 0) {
        property_len += SERVICE_MQTT_V5_PROPERTY_ID_LEN + SERVICE_MQTT_V5_RECEIVE_MAX_LEN;
    }

    /* User Properties */
    for (iter = 0; iter < MQTT5_ATTRIBUTE_MAX; iter++) {
        mqtt5_property_t *user_prop = list[iter];
        if (NULL != user_prop) {
            property_len += SERVICE_MQTT_V5_PROPERTY_ID_LEN;         /* len of user property ID */
            property_len += SERVICE_MQTT_V5_USER_PROPERTY_KEY_LEN;   /* len of user property key's len */
            property_len += user_prop->key.len;
            property_len += SERVICE_MQTT_V5_USER_PROPERTY_VALUE_LEN; /* len of user property value's len */
            property_len += user_prop->value.len;
        }
    }
    /* Response Topic */
    if (0 != general_prop->response_topic.len) {
        property_len += SERVICE_MQTT_V5_PROPERTY_ID_LEN + SERVICE_MQTT_V5_RESPONSE_TOPIC_LEN + general_prop->response_topic.len;
    }

    /* Corelation Data */
    if (0 != general_prop->correlation_data.len) {
        property_len += SERVICE_MQTT_V5_PROPERTY_ID_LEN + SERVICE_MQTT_V5_CORELATION_DATA_LEN + general_prop->correlation_data.len;
    }

    /* Subscription Identifier */
    if (general_prop->subscription_identifier > 0) {
        _service_mqtt_remain_len_encode(general_prop->subscription_identifier, &subscription_identifier[0],
                                     &subscription_identifer_offset);
        property_len += SERVICE_MQTT_V5_PROPERTY_ID_LEN + subscription_identifer_offset;
    }

    mqtt5_property_element_t *reason_string = general_prop->reason_string;

    if (NULL != reason_string) {
        property_len += reason_string->len + SERVICE_MQTT_V5_PROPERTY_ID_LEN + SERVICE_MQTT_V5_REASON_STRING_LEN;
    }

    return property_len;
}

static int32_t _service_mqtt_tx_topic_alias_process(service_mq_process_t *mq_process, service_mqtt_buff_t *topic,
        uint16_t *alias_id_prt
                                         )
{
    service_mqtt_topic_alias_node_t *topic_alias_node = NULL;
    uint16_t alias_id = 0;
    int32_t replace_topic_with_alias = 0;

    /* Topic talkweb */
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->topic_alias_mutex);
    service_list_for_each_entry(topic_alias_node, &mq_process->tx_topic_alias_list, linked_node,
                             service_mqtt_topic_alias_node_t) {
        if (memcmp(topic_alias_node->topic, topic->buffer, topic->len) == 0) {
            log_debug("tx use topic alias\n");
            alias_id = topic_alias_node->topic_alias;
            replace_topic_with_alias = 1;
            break;
        }
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->topic_alias_mutex);

    /* save the tx topic in tx_topic_alias_list */
    if (0 == replace_topic_with_alias) {
        alias_id = mq_process->tx_topic_alias;
        alias_id++;
        /* make sure that it does not exceed tx_topic_alias_max */
        if (alias_id <= mq_process->tx_topic_alias_max) {
            mq_process->tx_topic_alias = alias_id;
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->topic_alias_mutex);
            _service_mqtt_topic_alias_list_insert(mq_process, topic, alias_id, &(mq_process->tx_topic_alias_list));
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->topic_alias_mutex);
        } else {
            /* exceed tx_topic_alias_max, no more alias could be used */
            alias_id = 0;
        }
    }
    *alias_id_prt = alias_id;
    return replace_topic_with_alias;
}

static int32_t _service_mqtt_check_flow_control(service_mq_process_t *mq_process, uint8_t qos)
{
    if (mq_process->flow_control_enabled && qos == SERVICE_MQTT_QOS1) {
        uint16_t server_receive_max = 0;
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.pub_mutex);
        server_receive_max = mq_process->server_receive_max;
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.pub_mutex);

        if (server_receive_max <= 0) {
            return RET_MQTT_RECEIVE_MAX_EXCEEDED;
        }
    }
    return RET_SUCCESS;
}

static void _service_mqtt_check_flow_dec(service_mq_process_t *mq_process)
{
    if (_service_mqtt_5_feature_is_enabled(mq_process) && mq_process->flow_control_enabled
        && mq_process->server_receive_max > 0) {
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.pub_mutex);
        mq_process->server_receive_max--;
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.pub_mutex);
    }
}

static int32_t _service_mqtt_check_tx_payload_len(uint32_t pkt_len, service_mq_process_t *mq_process)
{
    if (pkt_len >= mq_process->tx_packet_max_size) {
        _service_mqtt_exec_dec(mq_process);
        return RET_MQTT_INVALID_TX_PACK_SIZE;
    }
    return RET_SUCCESS;
}

static int32_t _service_mqtt_pub(void *ctx, service_mqtt_buff_t *topic, service_mqtt_buff_t *payload, uint8_t qos,
                              pub_property_t *pub_prop)
{
    int32_t res = RET_SUCCESS;
    uint16_t packet_id = 0;
    uint8_t *pkt = NULL;
    uint32_t idx = 0, remainlen = 0, pkt_len = 0;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;
    uint16_t alias_id = 0;
    uint8_t replace_topic_with_alias = 0;
    uint32_t property_len = 0;
    uint8_t property_len_array[4] = {0};
    uint32_t property_len_offset = 0;
    general_property_t general_prop = {0};


    if (mq_process == NULL ||
        payload == NULL || payload->buffer == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    if (topic->len == 0 || qos > SERVICE_MQTT_QOS_MAX) {
        return RET_USER_INPUT_OUT_RANGE;
    }

    if (_service_mqtt_5_feature_is_enabled(mq_process)) {

        if (NULL != pub_prop) {
            memcpy(&(general_prop.user_property[0]), &(pub_prop->user_property[0]),
                   MQTT5_ATTRIBUTE_MAX * (sizeof(mqtt5_property_t *)));
            general_prop.response_topic = pub_prop->response_topic;
            general_prop.correlation_data = pub_prop->correlation_data;
            general_prop.subscription_identifier = pub_prop->subscription_identifier;
        }

        res = _service_mqtt_general_property_is_valid(&general_prop);
        if (res < RET_SUCCESS) {
            return res;
        }

        /* Flow Control Check*/
        res = _service_mqtt_check_flow_control(mq_process, qos);
        if (res < RET_SUCCESS) {
            return res;
        }

        /* Topic talkweb*/
        replace_topic_with_alias = _service_mqtt_tx_topic_alias_process(mq_process, topic, &alias_id);
        general_prop.topic_alias = alias_id;
    }

    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);

    log_debug("pub: %.*s", topic->len, topic->buffer);

    if (0 == replace_topic_with_alias) {
        remainlen = topic->len + payload->len + SERVICE_MQTT_UTF8_STR_EXTRA_LEN;
    } else {
        remainlen = payload->len + SERVICE_MQTT_UTF8_STR_EXTRA_LEN;
    }

    if (qos == SERVICE_MQTT_QOS1) {
        remainlen += SERVICE_MQTT_PACKETID_LEN;
    }

    if (_service_mqtt_5_feature_is_enabled(mq_process)) {
        property_len = _service_get_general_property_len(&general_prop);
        _service_mqtt_remain_len_encode(property_len, &property_len_array[0],
                                     &property_len_offset);
        remainlen += property_len + property_len_offset;
    }

    pkt_len = SERVICE_MQTT_FIXED_HEADER_LEN + SERVICE_MQTT_REMAINLEN_MAXLEN + remainlen;
    pkt = mq_process->sysdep->service_sysdep_malloc(pkt_len);
    if (pkt == NULL) {
        _service_mqtt_exec_dec(mq_process);
        return RET_SYS_DEPEND_MALLOC_FAILED;
    }
    memset(pkt, 0, pkt_len);

    /* Publish Packet Type */
    pkt[idx++] = SERVICE_MQTT_PUBLISH_PKT_TYPE | (qos << 1);

    /* Remaining Length */
    _service_mqtt_remain_len_encode(remainlen, &pkt[idx], &idx);

    /* Topic Length */
    if (replace_topic_with_alias == 0) {
        /* topic len is > 0 when alias DOES NOT replaces topic */
        _service_mqtt_set_utf8_encoded_str((uint8_t *)topic->buffer, topic->len, &pkt[idx]);
        idx += SERVICE_MQTT_UTF8_STR_EXTRA_LEN + topic->len;
    } else {
        /* topic len is 0 when alias replaces topic */
        pkt[idx++] = 0;
        pkt[idx++] = 0;
    }

    /* Packet Id For QOS 1*/
    if (qos == SERVICE_MQTT_QOS1) {
        packet_id = _service_mqtt_packet_id(ctx);
        pkt[idx++] = (uint8_t)((packet_id >> 8) & 0x00FF);
        pkt[idx++] = (uint8_t)((packet_id) & 0x00FF);
    }

    if (_service_mqtt_5_feature_is_enabled(mq_process)) {
        _service_write_general_prop(pkt, &idx,  &general_prop, property_len_array, property_len_offset);
    }

    /* Payload */
    memcpy(&pkt[idx], payload->buffer, payload->len);
    idx += payload->len;

    pkt_len = idx;

    /* Ensure tx pack size does not overflow  */
    res = _service_mqtt_check_tx_payload_len(pkt_len, mq_process);
    if (res < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_free(pkt);
        return res;
    }

    if (qos == SERVICE_MQTT_QOS1) {
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.pub_mutex);
        res = _service_mqtt_publist_insert(mq_process, pkt, pkt_len, packet_id);
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.pub_mutex);
        if (res < RET_SUCCESS) {
            mq_process->sysdep->service_sysdep_free(pkt);
            _service_mqtt_exec_dec(mq_process);
            return res;
        }
    }

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    res = _service_mqtt_write(ctx, pkt, pkt_len, mq_process->send_timeout_ms);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
    if (res < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_free(pkt);
        if (res != RET_SYS_DEPEND_NWK_WRITE_LESSDATA) {
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
            if (mq_process->network_ctx != NULL) {
                mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
            }
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
        }
        _service_mqtt_exec_dec(mq_process);
        return res;
    }

    mq_process->sysdep->service_sysdep_free(pkt);

    if (qos == SERVICE_MQTT_QOS1) {
        _service_mqtt_exec_dec(mq_process);
        _service_mqtt_check_flow_dec(mq_process);
        return (int32_t)packet_id;
    }

    _service_mqtt_exec_dec(mq_process);

    return RET_SUCCESS;
}

int32_t _service_mqtt_disconnect_with_prop(void *ctx,  uint8_t reason_code, disconn_property_t *discon_property)
{
    int32_t res = 0;
    uint32_t pkt_len = 0;
    uint8_t *pkt = NULL;
    uint32_t idx = 0;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;
    uint32_t property_len = 0;
    uint32_t property_len_offset = 0;
    uint8_t property_len_array[4] = {0};
    uint32_t pkt_len_offset = 0;
    uint8_t  pkt_len_array[4] = {0};
    uint32_t property_total_len = 0;
    uint32_t pkt_remain_len = 0;
    general_property_t general_property = {0};

    if (discon_property != NULL) {
        memcpy(&(general_property.user_property[0]), &(discon_property->user_property[0]),
               MQTT5_ATTRIBUTE_MAX * (sizeof(mqtt5_property_t *)));
        general_property.reason_string = discon_property->reason_string;
        int res = _service_mqtt_general_property_is_valid(&general_property);
        if (res < RET_SUCCESS) {
            return res;
        }
        property_len = _service_get_general_property_len(&general_property);
    }

    _service_mqtt_remain_len_encode(property_len, property_len_array, &property_len_offset);
    property_total_len = property_len_offset + property_len;

    pkt_remain_len = property_total_len + SERVICE_MQTT_V5_DISCONNECT_REASON_CODE_LEN;
    _service_mqtt_remain_len_encode(pkt_remain_len, pkt_len_array, &pkt_len_offset);

    pkt_len =  SERVICE_MQTT_FIXED_HEADER_LEN + pkt_len_offset + pkt_remain_len ;
    pkt = mq_process->sysdep->service_sysdep_malloc(pkt_len);
    if (NULL == pkt) {
        return RET_PORT_MALLOC_FAILED;
    }
    memset(pkt, 0, pkt_len);

    pkt[idx++] = SERVICE_MQTT_DISCONNECT_PKT_TYPE;
    memcpy(&pkt[idx], pkt_len_array, pkt_len_offset);
    idx += pkt_len_offset;
    pkt[idx++] = reason_code;
    _service_write_general_prop(&pkt[0], &idx, &general_property, property_len_array, property_len_offset);

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
    res = _service_mqtt_write(ctx, pkt, idx, mq_process->send_timeout_ms);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
    mq_process->sysdep->service_sysdep_free(pkt);

    return res;
}

static char* _mqtt_pub_with_json(void *ctx, char *payload, uint32_t payload_len, const uint8_t *req_method)
{
    cJSON * root =  cJSON_CreateObject();
    cJSON * spec =  cJSON_CreateObject();
    char reqid[32] = {0};
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    sprintf(reqid, "%ld", service_log_get_timestamp(mq_process->sysdep));
    cJSON_AddItemToObject(root, "reqid", cJSON_CreateString(reqid));
    cJSON_AddItemToObject(root, "v", cJSON_CreateString("1"));
    cJSON_AddItemToObject(root, "t", cJSON_CreateNumber(service_log_get_timestamp(mq_process->sysdep)));
    cJSON_AddItemToObject(root, "method", cJSON_CreateString(req_method));
    cJSON_AddItemToObject(spec, "ack", cJSON_CreateNumber(0));
    cJSON_AddItemToObject(root, "spec", spec);
    cJSON_AddItemToObject(root, "data", cJSON_Parse(payload));

    return cJSON_Print(root);
}

static int32_t _mqtt_pub_with_prop(void *ctx, char *topic, uint8_t *payload, uint32_t payload_len, uint8_t qos,
                                   pub_property_t *pub_prop, const uint8_t *req_method)
{
    service_mqtt_buff_t    topic_buff = {0};
    service_mqtt_buff_t    payload_buff;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;
    uint8_t append_rid = 0;
    int32_t res = RET_SUCCESS;
    char *json_string = NULL;

    if (NULL == topic || NULL == ctx) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    if (strlen(topic) >= SERVICE_MQTT_TOPIC_MAXLEN) {
        return RET_MQTT_TOPIC_TOO_LONG;
    }

    if (payload_len >= SERVICE_MQTT_PAYLOAD_MAXLEN) {
        return RET_MQTT_PUB_PAYLOAD_TOO_LONG;
    }

    memset(&topic_buff, 0, sizeof(topic_buff));
    memset(&payload_buff, 0, sizeof(payload_buff));

    append_rid = mq_process->append_requestid;

    if (0 == append_rid) {
        topic_buff.buffer = (uint8_t *)topic;
        topic_buff.len = strlen(topic);
    } else {
        char *rid_prefix = "?_rid=";
        uint64_t timestamp = service_log_get_timestamp(mq_process->sysdep);
        uint32_t rand = 0;
        char *buffer = NULL;
        uint32_t buffer_len = strlen(topic) + strlen(rid_prefix) + 32;

        buffer = mq_process->sysdep->service_sysdep_malloc(buffer_len);
        if (NULL == buffer) {
            return RET_PORT_MALLOC_FAILED;
        }
        memset(buffer, 0, buffer_len);

        memcpy(buffer, topic, strlen(topic));
        memcpy(buffer + strlen(buffer), rid_prefix, strlen(rid_prefix));
        service_uint642str(timestamp, buffer + strlen(buffer), NULL);
        mq_process->sysdep->service_sysdep_rand((uint8_t *)&rand, sizeof(rand));
        service_uint2str(rand, buffer + strlen(buffer), NULL);

        topic_buff.buffer = (uint8_t *)buffer;
        topic_buff.len = strlen(buffer);
    }
    if(req_method != NULL){
        json_string = _mqtt_pub_with_json(ctx, payload, strlen(payload), req_method);
        payload_buff.buffer = json_string;
        payload_buff.len = strlen(json_string);
    }

    res = _service_mqtt_pub(ctx, &topic_buff, &payload_buff, qos, pub_prop);
    if (append_rid != 0) {
        mq_process->sysdep->service_sysdep_free(topic_buff.buffer);
    }
    return res;
}

int32_t tiot_mqtt_pub_with_method(void *ctx, char *topic, uint8_t *payload, uint32_t payload_len, uint8_t qos, const uint8_t *pub_method)
{
    return  _mqtt_pub_with_prop(ctx, topic, payload, payload_len, qos, NULL, pub_method);
}

static int32_t _service_mqtt_sub(void *ctx_addr, service_mqtt_buff_t *topic, tiot_mqtt_recv_ctx_t ctx,
                              uint8_t qos,
                              void *userdata, sub_property_t *sub_prop)
{
    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx_addr;

    if (mq_process == NULL || topic == NULL || topic->buffer == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (topic->len == 0 || qos > SERVICE_MQTT_QOS_MAX) {
        return RET_USER_INPUT_OUT_RANGE;
    }
    if ((res = _service_mqtt_topic_is_valid((char *)topic->buffer, topic->len)) < RET_SUCCESS) {
        return RET_MQTT_TOPIC_INVALID;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);

    log_debug("sub: %.*s\r\n", topic->len, topic->buffer);

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.sub_mutex);
    res = _service_mqtt_sublist_insert(mq_process, topic, ctx, userdata);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.sub_mutex);

    if (res < RET_SUCCESS) {
        return res;
    }
    general_property_t general_property = {0};
    if (NULL != sub_prop) {
        memcpy(&(general_property.user_property[0]), &(sub_prop->user_property[0]),
               MQTT5_ATTRIBUTE_MAX * (sizeof(mqtt5_property_t *)));
    }

    /* send subscribe packet */
    res = _service_mqtt_subunsub(mq_process, (char *)topic->buffer, topic->len, qos, SERVICE_MQTT_SUB_PKT_TYPE,
                              &general_property);

    _service_mqtt_exec_dec(mq_process);

    return res;
}

static int32_t _mqtt_sub_with_prop(void *ctx_addr, char *topic, tiot_mqtt_recv_ctx_t ctx, uint8_t qos,
                                   void *userdata, sub_property_t *sub_prop)
{
    service_mqtt_buff_t    topic_buff;

    if (NULL == topic) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    if (strlen(topic) >= SERVICE_MQTT_TOPIC_MAXLEN) {
        return RET_MQTT_TOPIC_TOO_LONG;
    }

    memset(&topic_buff, 0, sizeof(topic_buff));

    topic_buff.buffer = (uint8_t *)topic;
    topic_buff.len = strlen(topic);

    return _service_mqtt_sub(ctx_addr, &topic_buff, ctx, qos, userdata, sub_prop);
}

int32_t tiot_mqtt_sub(void *ctx_addr, char *topic, tiot_mqtt_recv_ctx_t ctx, uint8_t qos, void *userdata)
{
    return _mqtt_sub_with_prop(ctx_addr, topic, ctx, qos, userdata, NULL);
}

static int32_t _tiot_mqtt_recv_resolve_data(void *ctx, char *msgdata, char **enddata)
{
    if (NULL == msgdata) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    cJSON *root = cJSON_Parse(msgdata);
    if(root == 0){
        return RET_JSON_PARSE_ERROR;
    }
    cJSON *data = cJSON_GetObjectItem(root, "data");
    if(data == 0){
        cJSON_Delete(root);
        return RET_JSON_PARSE_ERROR;
    }
    *enddata = cJSON_Print(data);
    cJSON_Delete(root);
    return RET_SUCCESS;
}

int32_t tiot_mqtt_sub_recv_data(void *ctx, char *msgdata, char **enddata)
{
    return _tiot_mqtt_recv_resolve_data(ctx, msgdata, enddata);
}

static int32_t _tiot_mqtt_recv_resolve_head(void *ctx, char *msgdata, char **enddata)
{
    if (NULL == msgdata) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    cJSON *root = cJSON_Parse(msgdata);
    if(root == 0){
        return RET_JSON_PARSE_ERROR;
    }
    cJSON *spec = cJSON_GetObjectItem(root, "spec");
    if(spec == 0){
        cJSON_Delete(root);
        return RET_JSON_PARSE_ERROR;
    }
    
    *enddata = cJSON_Print(spec);
    cJSON_Delete(root);
    return RET_SUCCESS;
}

int32_t tiot_mqtt_sub_recv_head(void *ctx, char *msgdata, char **enddata)
{
    return _tiot_mqtt_recv_resolve_head(ctx, msgdata, enddata);
}

int32_t tiot_mqtt_sub_with_prop(void *ctx_addr, char *topic, tiot_mqtt_recv_ctx_t ctx, uint8_t qos,
                                void *userdata, sub_property_t *sub_prop)
{
    if (ctx_addr == NULL || topic == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx_addr;

    return _mqtt_sub_with_prop(mq_process, topic, ctx, qos, userdata, sub_prop);
}

static int32_t _service_mqtt_unsub(void *ctx, service_mqtt_buff_t *topic, mtt5_unsub_property_t *unsub_prop)
{
    if (ctx == NULL || topic == NULL || topic->buffer == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    int32_t res = RET_SUCCESS;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (topic->len == 0) {
        return RET_USER_INPUT_OUT_RANGE;
    }
    if ((res = _service_mqtt_topic_is_valid((char *)topic->buffer, topic->len)) < RET_SUCCESS) {
        return RET_MQTT_TOPIC_INVALID;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);

    log_debug("unsub: %.*s\r\n", topic->len, topic->buffer);

    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.sub_mutex);
    _service_mqtt_sublist_remove(mq_process, topic);
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.sub_mutex);

    general_property_t general_property = {0};
    if (NULL != unsub_prop) {
        memcpy(&(general_property.user_property[0]), &(unsub_prop->user_property[0]),
               MQTT5_ATTRIBUTE_MAX * (sizeof(mqtt5_property_t *)));
    }

    res = _service_mqtt_subunsub(mq_process, (char *)topic->buffer, topic->len, 0, SERVICE_MQTT_UNSUB_PKT_TYPE,
                              &general_property);

    _service_mqtt_exec_dec(mq_process);

    return res;
}

static int32_t _mqtt_unsub(void *ctx, char *topic, mtt5_unsub_property_t *unsub_prop)
{
    if (topic == NULL || ctx == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    service_mqtt_buff_t topic_buff;

    if (strlen(topic) >= SERVICE_MQTT_TOPIC_MAXLEN) {
        return RET_MQTT_TOPIC_TOO_LONG;
    }

    memset(&topic_buff, 0, sizeof(topic_buff));

    topic_buff.buffer = (uint8_t *)topic;
    topic_buff.len = strlen(topic);

    return _service_mqtt_unsub(ctx, &topic_buff, unsub_prop);
}


int32_t tiot_mqtt_unsub(void *ctx, char *topic)
{
    return _mqtt_unsub(ctx, topic, NULL);
}

int32_t tiot_mqtt_unsub_with_prop(void *ctx, char *topic, mtt5_unsub_property_t *unsub_prop)
{
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL || topic == NULL || unsub_prop == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }

    return _mqtt_unsub(ctx, topic, unsub_prop);
}

int32_t tiot_mqtt_recv(void *ctx)
{
    int32_t res = RET_SUCCESS;
    uint32_t mqtt_remainlen = 0;
    uint8_t mqtt_fixed_header[SERVICE_MQTT_FIXED_HEADER_LEN] = {0};
    uint8_t mqtt_pkt_type = 0, mqtt_pkt_reserved = 0;
    uint8_t *remain = NULL;
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (mq_process == NULL) {
        return RET_USER_INPUT_NULL_POINTER;
    }
    if (mq_process->exec_enabled == 0) {
        return RET_USER_INPUT_EXEC_DISABLED;
    }

    _service_mqtt_exec_inc(mq_process);

    /* network error reconnect */
    if (mq_process->network_ctx == NULL) {
        _service_mqtt_disconnect_event_notify(mq_process, TIOT_MQTTDISCONNEVT_NETWORK_DISCONNECT);
    }
    if (mq_process->reconnect_params.enabled == 1 && mq_process->cn_flag.disconn_called == 0) {
        res = _service_mqtt_reconnect(mq_process);
        if (res < RET_SUCCESS) {
            if (res == RET_MQTT_CONNECT_SUCCESS) {
                mq_process->heartbeat_params.lost_times = 0;
                log_debug("MQTT network reconnect success\r\n");
                _service_mqtt_connect_event_notify(mq_process);
                res = RET_SUCCESS;
            } else {
                _service_mqtt_exec_dec(mq_process);
                return res;
            }
        }
    }

    /* heartbeat missing reconnect */
    if (mq_process->heartbeat_params.lost_times > mq_process->heartbeat_params.max_lost_times) {
        _service_mqtt_disconnect_event_notify(mq_process, TIOT_MQTTDISCONNEVT_HEARTBEAT_DISCONNECT);
        if (mq_process->reconnect_params.enabled == 1 && mq_process->cn_flag.disconn_called == 0) {
            log_debug("MQTT heartbeat lost %d times, try to reconnecting...\r\n",
                      mq_process->heartbeat_params.lost_times);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
            res = _service_mqtt_connect(mq_process, NULL);
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
            if (res < RET_SUCCESS) {
                if (res == RET_MQTT_CONNECT_SUCCESS) {
                    mq_process->heartbeat_params.lost_times = 0;
                    log_debug("MQTT network reconnect success\r\n");
                    _service_mqtt_connect_event_notify(mq_process);
                    res = RET_SUCCESS;
                } else {
                    _service_mqtt_exec_dec(mq_process);
                    return res;
                }
            }
        }
    }

    /* Read Packet Type Of MQTT Fixed Header, Get  And Remaining Packet Length */
    mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
    res = _service_mqtt_read(ctx, mqtt_fixed_header, SERVICE_MQTT_FIXED_HEADER_LEN, mq_process->recv_timeout_ms);
    if (res < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
        _service_mqtt_exec_dec(mq_process);
        if (res == RET_SYS_DEPEND_NWK_READ_LESSDATA) {
            res = RET_SUCCESS;
        } else {
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
            mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
            if (mq_process->network_ctx != NULL) {
                mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
            }
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
            mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
        }
        return res;
    }

    /* Read Remaining Packet Length Of MQTT Fixed Header */
    res = _service_mqtt_read_remainlen(mq_process, &mqtt_remainlen);
    if (res < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
        if (mq_process->network_ctx != NULL) {
            mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
        }
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
        _service_mqtt_exec_dec(mq_process);
        return res;
    }

    /* Read Remaining Bytes */
    res = _service_mqtt_read_remainbytes(ctx, mqtt_remainlen, &remain);

    if (res < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
        if (mq_process->network_ctx != NULL) {
            mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
        }
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
        _service_mqtt_exec_dec(mq_process);
        return res;
    }
    mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);

    /* Get Packet Type */
    mqtt_pkt_type = mqtt_fixed_header[0] & 0xF0;
    mqtt_pkt_reserved = mqtt_fixed_header[0] & 0x0F;

    /* reset ping response missing times */
    mq_process->heartbeat_params.lost_times = 0;

    switch (mqtt_pkt_type) {
        case SERVICE_MQTT_PINGRESP_PKT_TYPE: {
            res = _service_mqtt_pingresp_ctx(mq_process, remain, mqtt_remainlen);
        }
        break;
        case SERVICE_MQTT_PUBLISH_PKT_TYPE: {
            res = _service_mqtt_pub_ctx(ctx, remain, mqtt_remainlen, ((mqtt_pkt_reserved >> 1) & 0x03), mqtt_remainlen);
        }
        break;
        case SERVICE_MQTT_PUBACK_PKT_TYPE: {
            res = _service_mqtt_puback_ctx(ctx, remain, mqtt_remainlen);
        }
        break;
        case SERVICE_MQTT_SUBACK_PKT_TYPE: {
            _service_mqtt_subunsuback_ctx(ctx, remain, mqtt_remainlen, SERVICE_MQTT_SUBACK_PKT_TYPE);
        }
        break;
        case SERVICE_MQTT_UNSUBACK_PKT_TYPE: {
            _service_mqtt_subunsuback_ctx(ctx, remain, mqtt_remainlen, SERVICE_MQTT_UNSUBACK_PKT_TYPE);
        }
        break;
        case SERVICE_MQTT_SERVER_DISCONNECT_PKT_TYPE: {
            _service_mqtt_server_disconnect_ctx(ctx, remain, mqtt_remainlen);
        }
        break;

        case SERVICE_MQTT_PUBREC_PKT_TYPE:
        case SERVICE_MQTT_PUBREL_PKT_TYPE:
        case SERVICE_MQTT_PUBCOMP_PKT_TYPE: {

        }
        break;
        default: {
            res = RET_MQTT_PACKET_TYPE_UNKNOWN;
        }
    }

    if (remain) {
        mq_process->sysdep->service_sysdep_free(remain);
    }

    if (res < RET_SUCCESS) {
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.send_mutex);
        mq_process->sysdep->service_sysdep_mutex_lock(mq_process->all_mutex.recv_mutex);
        if (mq_process->network_ctx != NULL) {
            mq_process->sysdep->service_sysdep_network_deinit(&mq_process->network_ctx);
        }
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.recv_mutex);
        mq_process->sysdep->service_sysdep_mutex_unlock(mq_process->all_mutex.send_mutex);
    }

    _service_mqtt_exec_dec(mq_process);

    return res;
}

char *service_mqtt_get_product_key(void *ctx)
{
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (ctx == NULL) {
        return NULL;
    }

    return mq_process->product_key;
}

char *service_mqtt_get_device_name(void *ctx)
{
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (ctx == NULL) {
        return NULL;
    }

    return mq_process->device_name;
}

uint16_t service_mqtt_get_port(void *ctx)
{
    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    if (ctx == NULL) {
        return 0;
    }

    return mq_process->port;
}

int32_t service_mqtt_get_nwkstats(void *ctx, service_mqtt_nwkstats_info_t *nwk_stats_info)
{
    if (ctx == NULL) {
        return 0;
    }

    service_mq_process_t *mq_process = (service_mq_process_t *)ctx;

    mq_process->sysdep->service_sysdep_memcpy(nwk_stats_info, &mq_process->nwkstats_info, sizeof(service_mqtt_nwkstats_info_t));
    mq_process->nwkstats_info.failed_error_code = 0;
    mq_process->nwkstats_info.connect_time_used = 0;
    return RET_SUCCESS;
}

