/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
/*
* http测试程序
*/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "tiot_http_api.h"
#include "tiot_sysdep_api.h"
#include "tiot_state_api.h"

extern tiot_sysdep_portfile_t g_tiot_sysdep_portfile;

int32_t demo_state_logcb(int32_t code, char *message)
{
    printf("%s", message);
    return 0;
}

void demo_http_event_ctx(void *ctx, const tiot_http_event_t *event, void *user_data)
{
    int32_t res;

    if (event->type == TIOT_HTTPEVT_TOKEN_INVALID) {
        printf("token invalid\n");
        res = tiot_http_auth(ctx);
    }
}

void demo_http_recv_ctx(void *ctx, const tiot_http_recv_t *packet, void *userdata)
{
    printf("packet->type : %d\n", packet->type);
    if(packet->type==TIOT_HTTPRECV_BODY) {
        printf("demo_http_recv_ctx recv bady:%.*s\r\n", packet->data.body.len, packet->data.body.buffer);
    }
}

int main(int argc, char *argv[])
{
    void *http_ctx = NULL;
    int32_t res;

    tiot_http_config_t http_config_ctx = 
        {.device_secret = "5da4d39b8030c6f0",
        .device_name = "123",
        .product_key = "r0SPgcPsc"
        };

    tiot_sysdep_set_portfile(&g_tiot_sysdep_portfile);
    tiot_set_print_callback(demo_state_logcb);

    res = tlink_sdk_http_init(&http_config_ctx, demo_http_recv_ctx, demo_http_event_ctx);
    if(res == 0){
        printf("http success\r\n");
    }else{
        printf("http error res:%x\r\n", res);
    }

    return 0;
}

