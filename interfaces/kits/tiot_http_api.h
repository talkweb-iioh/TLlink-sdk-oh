/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _TIOT_HTTP_API_H_
#define _TIOT_HTTP_API_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "service_stdinc.h"

/**
 * @brief 错误码
 *
 */
typedef enum {
    TIOT_HTTP_RSPCODE_SUCCESS                   = 0,
    TIOT_HTTP_RSPCODE_COMMON_ERROR              = 10000,
    TIOT_HTTP_RSPCODE_PARAM_ERROR               = 10001,
    TIOT_HTTP_RSPCODE_TOKEN_EXPIRED             = 20001,
    TIOT_HTTP_RSPCODE_TOKEN_NULL                = 20002,
    TIOT_HTTP_RSPCODE_TOKEN_CHECK_ERROR         = 20003,
    TIOT_HTTP_RSPCODE_PUBLISH_MESSAGE_ERROR     = 30001,
    TIOT_HTTP_RSPCODE_REQUEST_TOO_MANY          = 40000,
} tiot_http_response_code_t;

typedef enum {
    TIOT_HTTPOPT_HOST,
    TIOT_HTTPOPT_PORT,
    TIOT_HTTPOPT_NETWORK_CRED,
    TIOT_HTTPOPT_CONNECT_TIMEOUT_MS,
    TIOT_HTTPOPT_SEND_TIMEOUT_MS,
    TIOT_HTTPOPT_RECV_TIMEOUT_MS,
    TIOT_HTTPOPT_DEINIT_TIMEOUT_MS,
    TIOT_HTTPOPT_HEADER_BUFFER_LEN,
    TIOT_HTTPOPT_BODY_BUFFER_LEN,
    TIOT_HTTPOPT_EVENT_HANDLER,

    /* 以上选项配置的数据与 SERVICE_HTTPOPT_XXX 共用 */
    TIOT_HTTPOPT_USERDATA,
    TIOT_HTTPOPT_RECV_HANDLER,
    TIOT_HTTPOPT_PRODUCT_KEY,
    TIOT_HTTPOPT_DEVICE_NAME,
    TIOT_HTTPOPT_DEVICE_SECRET,
    TIOT_HTTPOPT_EXTEND_DEVINFO,
    TIOT_HTTPOPT_AUTH_TIMEOUT_MS,
    TIOT_HTTPOPT_LONG_CONNECTION,
    TIOT_HTTPOPT_MAX
} tiot_http_option_t;

typedef enum {
    TIOT_HTTPRECV_STATUS_CODE,
    TIOT_HTTPRECV_HEADER,
    TIOT_HTTPRECV_BODY
} tiot_http_recv_type_t;

typedef struct {
    tiot_http_recv_type_t type;
    union {
        struct {
            uint32_t code;
        } status_code;
        struct {
            char *key;
            char *value;
        } header;
        struct {
            uint8_t *buffer;
            uint32_t len;
        } body;
    } data;
} tiot_http_recv_t;

typedef void (*tiot_http_recv_ctx_t)(void *handle, const tiot_http_recv_t *packet, void *userdata);

typedef enum {
    TIOT_HTTPEVT_TOKEN_INVALID
} tiot_http_event_type_t;

typedef struct {
    tiot_http_event_type_t type;
} tiot_http_event_t;

typedef struct {
    uint16_t port;
    char *device_secret;
    char *device_name;
    char *product_key;
    char *http_host;
} tiot_http_config_t;

typedef void (* tiot_http_event_ctx_t)(void *handle, const tiot_http_event_t *event, void *userdata);

void *tiot_http_init(void);

int32_t tiot_http_setopt(void *handle, tiot_http_option_t option, void *data);

int32_t tiot_http_auth(void *handle);

int32_t tiot_http_send(void *handle, char *topic, uint8_t *payload, uint32_t payload_len);

int32_t tiot_http_recv(void *handle);

int32_t tiot_http_deinit(void **p_handle);

int32_t tlink_sdk_http_init(tiot_http_config_t *http_config, void *http_recv_ctx, void *http_event_ctx);


#if defined(__cplusplus)
}
#endif

#endif /* #ifndef _tiot_HTTP_API_H_ */

