/**
 * @file tiot_subdev_api.h
 * @brief subdev模块头文件, 提供子设备管理的能力
 *
 * @copyright Copyright (C) 2015-2020 Alibaba Group Holding Limited
 *
 */
#ifndef __tiot_SUBDEV_API_H__
#define __tiot_SUBDEV_API_H__

#if defined(__cplusplus)
extern "C" {
#endif

/**
 * @brief 子设备信息
 * 
 */
typedef struct {
    char *product_key;
    char *device_name;
} tiot_subdev_dev_t;


#if defined(__cplusplus)
}
#endif

#endif  /* __tiot_SUBDEV_API_H__ */

