/*
* Copyright (c) 2021 Talkweb Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _TIOT_STATE_APIS_H_
#define _TIOT_STATE_APIS_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

typedef int32_t (* tiot_state_logcb_t)(int32_t code, char *message);
int32_t tiot_set_print_callback(tiot_state_logcb_t ctx);

#define RET_SUCCESS                                               (0x0000)
#define RET_USER_INPUT_BASE                                       (-0x0100)
#define RET_USER_INPUT_NULL_POINTER                               (-0x0101)
#define RET_USER_INPUT_OUT_RANGE                                  (-0x0102)
#define RET_USER_INPUT_UNKNOWN_OPTION                             (-0x0103)
#define RET_USER_INPUT_MISSING_PRODUCT_KEY                        (-0x0104)
#define RET_USER_INPUT_MISSING_DEVICE_NAME                        (-0x0105)
#define RET_USER_INPUT_MISSING_DEVICE_SECRET                      (-0x0106)
#define RET_USER_INPUT_MISSING_PRODUCT_SECRET                     (-0x0107)
#define RET_USER_INPUT_MISSING_HOST                               (-0x0108)
#define RET_USER_INPUT_EXEC_DISABLED                              (-0x0109)
#define RET_USER_INPUT_JSON_PARSE_FAILED                          (-0x010A)
#define RET_SYS_DEPEND_BASE                                       (-0x0200)
#define RET_SYS_DEPEND_MALLOC_FAILED                              (-0x0201)
#define RET_SYS_DEPEND_NWK_INVALID_OPTION                         (-0x0202)
#define RET_SYS_DEPEND_NWK_EST_FAILED                             (-0x0203)
#define RET_SYS_DEPEND_NWK_CLOSED                                 (-0x0204)
#define RET_SYS_DEPEND_NWK_READ_LESSDATA                          (-0x0205)
#define RET_SYS_DEPEND_NWK_WRITE_LESSDATA                         (-0x0206)
#define RET_SYS_DEPEND_NWK_READ_OVERTIME                          (-0x0207)
#define RET_SYS_DEPEND_NWK_INVALID_CRED                           (-0x0208)
#define RET_SYS_DEPEND_NWK_SEND_ERR                               (-0x0209)
#define RET_SYS_DEPEND_NWK_RECV_ERR                               (-0x020A)
#define RET_MQTT_BASE                                             (-0x0300)
#define RET_MQTT_CONNACK_FMT_ERROR                                (-0x0301)
#define RET_MQTT_CONNACK_RCODE_UNACCEPTABLE_PROTOCOL_VERSION      (-0x0302)
#define RET_MQTT_CONNACK_RCODE_SERVER_UNAVAILABLE                 (-0x0303)
#define RET_MQTT_CONNACK_RCODE_BAD_USERNAME_PASSWORD              (-0x0304)
#define RET_MQTT_CONNACK_RCODE_NOT_AUTHORIZED                     (-0x0305)
#define RET_MQTT_CONNACK_RCODE_UNKNOWN                            (-0x0306)
#define RET_MQTT_PUBLIST_PACKET_ID_ROLL                           (-0x0307)
#define RET_MQTT_TOPIC_INVALID                                    (-0x0308)
#define RET_MQTT_LOG_TOPIC                                        (-0x0309)
#define RET_MQTT_LOG_HEXDUMP                                      (-0x030A)
#define RET_MQTT_CONNECT_SUCCESS                                  (-0x030B)
#define RET_MQTT_MALFORMED_REMAINING_LEN                          (-0x030C)
#define RET_MQTT_MALFORMED_REMAINING_BYTES                        (-0x030D)
#define RET_MQTT_PACKET_TYPE_UNKNOWN                              (-0x030E)
#define RET_MQTT_SUBACK_RCODE_FAILURE                             (-0x030F)
#define RET_MQTT_SUBACK_RCODE_UNKNOWN                             (-0x0310)
#define RET_MQTT_TOPIC_COMPARE_FAILED                             (-0x0311)
#define RET_MQTT_DEINIT_TIMEOUT                                   (-0x0312)
#define RET_MQTT_LOG_CONNECT                                      (-0x0313)
#define RET_MQTT_LOG_RECONNECTING                                 (-0x0314)
#define RET_MQTT_LOG_CONNECT_TIMEOUT                              (-0x0315)
#define RET_MQTT_LOG_DISCONNECT                                   (-0x0316)
#define RET_MQTT_LOG_USERNAME                                     (-0x0317)
#define RET_MQTT_LOG_PASSWORD                                     (-0x0318)
#define RET_MQTT_LOG_CLIENTID                                     (-0x0319)
#define RET_MQTT_LOG_TLS_PSK                                      (-0x031A)
#define RET_MQTT_TOPIC_TOO_LONG                                   (-0x031B)
#define RET_MQTT_PUB_PAYLOAD_TOO_LONG                             (-0x031C)
#define RET_MQTT_LOG_BACKUP_IP                                    (-0x031D)
#define RET_MQTT_RECV_INVALID_PINRESP_PACKET                      (-0x031E)
#define RET_MQTT_RECV_INVALID_PUBLISH_PACKET                      (-0x031F)
#define RET_MQTT_RECV_INVALID_PUBACK_PACKET                       (-0x0320)
#define RET_MQTT_UNKNOWN_PROPERTY_OPTION                          (-0x0321)
#define RET_MQTT_INVALID_PROTOCOL_VERSION                         (-0x0322)
#define RET_MQTT_RECV_INVALID_SERVER_DISCONNECT_PACKET            (-0x0323)
#define RET_MQTT_INVALID_USER_PERPERTY_DATA                       (-0x0324)
#define RET_MQTT_INVALID_USER_PERPERTY_LEN                        (-0x0325)
#define RET_MQTT_INVALID_TX_PACK_SIZE                             (-0x0326)
#define RET_MQTT_RECEIVE_MAX_EXCEEDED                             (-0x0327)
#define RET_MQTT_INVALID_SUBSCRIPTION_IDENTIFIER                  (-0x0328)
#define RET_MQTT_INVALID_PROPERTY_LEN                             (-0x0329)
#define RET_HTTP_BASE                                             (-0x0400)
#define RET_HTTP_STATUS_LINE_INVALID                              (-0x0401)
#define RET_HTTP_READ_BODY_FINISHED                               (-0x0402)
#define RET_HTTP_DEINIT_TIMEOUT                                   (-0x0403)
#define RET_HTTP_AUTH_CODE_FAILED                                 (-0x0404)
#define RET_HTTP_AUTH_NOT_FINISHED                                (-0x0405)
#define RET_HTTP_AUTH_TOKEN_FAILED                                (-0x0406)
#define RET_HTTP_NEED_AUTH                                        (-0x0407)
#define RET_HTTP_RECV_NOT_FINISHED                                (-0x0408)
#define RET_HTTP_HEADER_BUFFER_TOO_SHORT                          (-0x0409)
#define RET_HTTP_HEADER_INVALID                                   (-0x040A)
#define RET_HTTP_LOG_SEND_HEADER                                  (-0x040B)
#define RET_HTTP_LOG_SEND_CONTENT                                 (-0x040C)
#define RET_HTTP_LOG_RECV_HEADER                                  (-0x040D)
#define RET_HTTP_LOG_RECV_CONTENT                                 (-0x040E)
#define RET_HTTP_LOG_DISCONNECT                                   (-0x040F)
#define RET_HTTP_LOG_AUTH                                         (-0x0410)
#define RET_HTTP_AUTH_NOT_EXPECTED                                (-0x0411)
#define RET_HTTP_READ_BODY_EMPTY                                  (-0x0412)
#define RET_PORT_BASE                                             (-0x0F00)
#define RET_PORT_INPUT_NULL_POINTER                               (-0x0F01)
#define RET_PORT_INPUT_OUT_RANGE                                  (-0x0F02)
#define RET_PORT_MALLOC_FAILED                                    (-0x0F03)
#define RET_PORT_MISSING_HOST                                     (-0x0F04)
#define RET_PORT_TCP_CLIENT_NOT_IMPLEMENT                         (-0x0F05)
#define RET_PORT_TCP_SERVER_NOT_IMPLEMENT                         (-0x0F06)
#define RET_PORT_UDP_CLIENT_NOT_IMPLEMENT                         (-0x0F07)
#define RET_PORT_UDP_SERVER_NOT_IMPLEMENT                         (-0x0F08)
#define RET_PORT_NETWORK_UNKNOWN_OPTION                           (-0x0F09)
#define RET_PORT_NETWORK_UNKNOWN_SOCKET_TYPE                      (-0x0F0A)
#define RET_PORT_NETWORK_DNS_FAILED                               (-0x0F0B)
#define RET_PORT_NETWORK_SOCKET_CREATE_FAILED                     (-0x0F0C)
#define RET_PORT_NETWORK_SOCKET_CONFIG_FAILED                     (-0x0F0D)
#define RET_PORT_NETWORK_SOCKET_BIND_FAILED                       (-0x0F0E)
#define RET_PORT_NETWORK_CONNECT_TIMEOUT                          (-0x0F0F)
#define RET_PORT_NETWORK_CONNECT_FAILED                           (-0x0F10)
#define RET_PORT_NETWORK_SELECT_FAILED                            (-0x0F11)
#define RET_PORT_NETWORK_SEND_FAILED                              (-0x0F12)
#define RET_PORT_NETWORK_RECV_FAILED                              (-0x0F13)
#define RET_PORT_NETWORK_SEND_CONNECTION_CLOSED                   (-0x0F14)
#define RET_PORT_NETWORK_RECV_CONNECTION_CLOSED                   (-0x0F15)
#define RET_PORT_TLS_INVALID_CRED_OPTION                          (-0x0F16)
#define RET_PORT_TLS_INVALID_MAX_FRAGMENT                         (-0x0F17)
#define RET_PORT_TLS_INVALID_SERVER_CERT                          (-0x0F18)
#define RET_PORT_TLS_INVALID_CLIENT_CERT                          (-0x0F19)
#define RET_PORT_TLS_INVALID_CLIENT_KEY                           (-0x0F1A)
#define RET_PORT_TLS_SOCKET_CREATE_FAILED                         (-0x0F1B)
#define RET_PORT_TLS_SOCKET_CONNECT_FAILED                        (-0x0F1C)
#define RET_PORT_TLS_INVALID_RECORD                               (-0x0F1D)
#define RET_PORT_TLS_RECV_FAILED                                  (-0x0F1E)
#define RET_PORT_TLS_SEND_FAILED                                  (-0x0F1F)
#define RET_PORT_TLS_RECV_CONNECTION_CLOSED                       (-0x0F20)
#define RET_PORT_TLS_SEND_CONNECTION_CLOSED                       (-0x0F21)
#define RET_PORT_TLS_CONFIG_PSK_FAILED                            (-0x0F22)
#define RET_PORT_TLS_INVALID_HANDSHAKE                            (-0x0F23)
#define RET_PORT_DTLS_CONFIG_PSK_FAILED                           (-0x0F24)
#define RET_PORT_DTLS_HANDSHAKE_FAILED                            (-0x0F25)
#define RET_PORT_NETWORK_DTLS_CONNECT_FAILED                      (-0x0F26)
#define RET_PORT_DTLS_HANDSHAKE_IN_PROGRESS                       (-0x0F27)
#define RET_JSON_PARSE_ERROR                                      (-0x0F28)
#define RET_HTTP_READ_BODY_FINISHED_CHUNCED                       (-0x0F29)
#define RET_HTTP_INIT_ERROR                                       (-0x0F2A)
#define RET_ADAPTER_COMMON                                        (-0x0F2B)

#if defined(__cplusplus)
}
#endif

#endif /* #ifndef _tiot_state_api_H_ */

