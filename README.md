# c-sdk

#### 介绍
基于OpenHarmony的终端设备SDK，可用于鸿蒙设备和模组

#### 软件架构
```
iot-device-sdk-c
├── adapter
├── figures
├── frameworks
├── interfaces
│   ├── innerkits
│   └── kits
├── services
├── test
└── README.md
├── secret

adapter：平台适配代码存放目录。
figures：README.md描述文件的媒体文件存放目录，比如图片文件。
frameworks：核心框架代码存放目录。
interfaces：对外接口存放目录，包括面向开发者的SDK API接口和面向系统内部其他子系统的API接口。
interfaces/innerkits：面向系统内部其他子系统的对外API接口存放目录。
interfaces/kits：面向开发者的SDK API接口存放目录，此目录下的接口要保持稳定性。
services：服务框架实现代码存放目录，此部分代码基本上和SAMGR有关。
test：模块测试用例代码存放目录。
README.md：模块介绍描述文件，介绍了此模块的一些基本信息，也是学习此模块的重要资料。
secret：放置密匙
```

#### 安装教程

1.  make
2.  ./out/mqtt-test 

#### 使用说明

详情参看[《sdk详细使用步骤.md》](docs/sdk详细使用步骤.md)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技





